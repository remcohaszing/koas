import { User } from '../models/index.js';

/**
 * Authenticate a user using HTTP basic authentication.
 *
 * @param username The username the user entered.
 * @param password The password the user entered.
 * @returns The logged in user, if any.
 */
export function basicAuth(username: string, password: string): Promise<User> {
  return User.findOne({ where: { username, password } });
}
