import { User } from '../models/index.js';

/**
 * Authenticate a user using an API key in the `apiKey` cookie.
 *
 * @param apiKey The API key that given.
 * @returns The authenticated user.
 */
export function cookieAuth(apiKey: string): Promise<User> {
  return User.findOne({ where: { apiKey } });
}
