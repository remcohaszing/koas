import { User } from '../models/index.js';

/**
 * Authenticate a user using an API key in a `apiKey` header.
 *
 * @param apiKey The API key that given.
 * @returns The authenticated user.
 */
export function headerAuth(apiKey: string): Promise<User> {
  return User.findOne({ where: { apiKey } });
}
