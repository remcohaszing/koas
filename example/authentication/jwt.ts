import { verify } from 'jsonwebtoken';
import { type Context } from 'koa';

import { User } from '../models/index.js';
import { type JWTPayload } from '../types.js';

/**
 * Authenticate a user using an access token.
 *
 * @param accessToken The access token in the Authorization header.
 * @param ctx The Koa context.
 * @returns The logged in user, if any.
 */
export function jwt(accessToken: string, ctx: Context): Promise<User> {
  let payload: JWTPayload;
  try {
    payload = verify(accessToken, ctx.secret) as JWTPayload;
  } catch {
    return null;
  }
  return User.findOne({ where: { username: payload.sub } });
}
