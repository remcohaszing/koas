import { User } from '../models/index.js';

/**
 * Authenticate a user using an API key in the `apiKey` query parameter.
 *
 * @param apiKey The API key that given.
 * @returns The authenticated user.
 */
export function queryAuth(apiKey: string): Promise<User> {
  return User.findOne({ where: { apiKey } });
}
