import { request, setTestApp } from 'axios-test-instance';

import { createServer } from '../server.js';
import { type Pet, type TokenResponse } from '../types.js';

beforeEach(async () => {
  const app = await createServer({ database: 'sqlite://:memory:', secret: 'test' });
  await setTestApp(app);
  const { data } = await request.post<TokenResponse>('/register', {
    username: 'user',
    password: 'password',
  });
  request.defaults.headers.common.authorization = `Bearer ${data.accessToken}`;
});

afterEach(() => {
  delete request.defaults.headers.common.authorization;
});

it('should be possible to create pets', async () => {
  const { data, status } = await request.post('/pets', { species: 'cat', name: 'Mittens' });
  expect(data).toStrictEqual({ id: expect.any(Number), species: 'cat', name: 'Mittens' });
  expect(status).toBe(201);
});

it('should be possible to retrieve pets', async () => {
  const { data: created } = await request.post<Pet>('/pets', { species: 'cat', name: 'Mittens' });
  const { data: retrieved, status } = await request.get(`/pets/${created.id}`);
  expect(retrieved).toStrictEqual(created);
  expect(status).toBe(200);
});

it('should be possible to create and list', async () => {
  const { data: cat } = await request.post('/pets', { species: 'cat', name: 'Mittens' });
  const { data: dog } = await request.post('/pets', { species: 'dog', name: 'Brian' });
  const { data, status } = await request.get('/pets');
  expect(data).toStrictEqual([cat, dog]);
  expect(status).toBe(200);
});
