import { type Context } from 'koa';

import { Pet } from '../models/index.js';

/**
 * Create a pet.
 *
 * @param ctx The Koa context.
 */
export async function createPet(ctx: Context): Promise<void> {
  ctx.body = await Pet.create(ctx.request.body);
}

/**
 * Get all pets.
 *
 * @param ctx The Koa context.
 */
export async function getPets(ctx: Context): Promise<void> {
  ctx.body = await Pet.findAll({ raw: true });
}

/**
 * Get a single pt.
 *
 * @param ctx The Koa context.
 */
export async function getPetById(ctx: Context): Promise<void> {
  const { petId } = ctx.params;
  ctx.body = await Pet.findByPk(petId, { raw: true });
}

/**
 * Update a pet.
 *
 * @param ctx The Koa context.
 */
export async function patchPet(ctx: Context): Promise<void> {
  const { petId } = ctx.params;
  const pet = await Pet.findByPk(petId);
  pet.set(ctx.request.body);
  await pet.save();
  ctx.body = pet;
}

/**
 * Update a pet.
 *
 * @param ctx The Koa context.
 */
export async function updatePet(ctx: Context): Promise<void> {
  const { petId } = ctx.params;
  const pet = await Pet.findByPk(petId);
  pet.set(ctx.request.body);
  await pet.save();
  ctx.body = pet;
}

/**
 * Delete a pet.
 *
 * @param ctx The Koa context.
 */
export async function deletePet(ctx: Context): Promise<void> {
  const { petId } = ctx.params;
  const pet = await Pet.findByPk(petId);
  await pet.destroy();
}
