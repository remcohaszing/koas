import { randomBytes } from 'node:crypto';

import { sign } from 'jsonwebtoken';
import { type Context } from 'koa';

import { User } from '../models/index.js';
import { type PasswordCredentials } from '../types.js';

/**
 * Register a new user.
 *
 * @param ctx The Koa context.
 */
export async function registerUser(ctx: Context): Promise<void> {
  const { password, username } = ctx.request.body as PasswordCredentials;
  const apiKey = String(randomBytes(12));
  await User.create({ username, password, apiKey });
  const accessToken = sign(
    {
      exp: Math.floor(Date.now() / 1000) + 3600,
      sub: username,
    },
    ctx.secret,
  );
  ctx.body = {
    username,
    apiKey,
    accessToken,
  };
}
