import 'reflect-metadata';
import * as yargs from 'yargs';

import { start } from './server.js';

/**
 * Run the application.
 */
async function main(): Promise<void> {
  const argv = await yargs
    .usage('Usage:\n  npm start')
    .option('database', {
      desc: 'The database URL to use.',
      default: 'sqlite://:memory:',
    })
    .option('port', {
      desc: 'The port on which to host the server.',
      type: 'number',
      default: 3333,
    })
    .option('secret', {
      desc: 'The app secret.',
      default: 'super-secret',
    }).argv;
  await start(argv);
}

if (module === require.main) {
  // eslint-disable-next-line unicorn/prefer-top-level-await
  main().catch((error) => {
    // eslint-disable-next-line no-console
    console.error(error);
    process.exit(error.code || 1);
  });
}
