import { BelongsTo, Column, ForeignKey, Model, PrimaryKey, Table } from 'sequelize-typescript';

import { User } from './index.js';

/**
 * An OAuth2 client that may act on behalf of a user.
 */
@Table
export class Client extends Model {
  /**
   * The OAuth2 client ID.
   */
  @PrimaryKey
  @Column
  id: string;

  /**
   * The OAuth2 client secret.
   */
  @Column
  secret: string;

  /**
   * The user ID represented by this client.
   */
  @ForeignKey(() => User)
  @Column
  UserId: number;

  /**
   * The user represented by this client.
   */
  @BelongsTo(() => User)
  User: User;
}
