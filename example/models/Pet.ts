import { AutoIncrement, Column, Model, PrimaryKey, Table } from 'sequelize-typescript';

/**
 * A pet.
 */
@Table
export class Pet extends Model {
  /**
   * The unique identifier of the pet.
   */
  @PrimaryKey
  @AutoIncrement
  @Column
  id: number;

  /**
   * The name of the pet.
   */
  @Column
  name?: string;

  /**
   * The species of the pet.
   *
   * @example 'cat'
   */
  @Column
  species?: string;
}
