import { Column, Model, PrimaryKey, Table } from 'sequelize-typescript';

/**
 * A user who may authenticate to the API.
 */
@Table
export class User extends Model {
  /**
   * The username of the user.
   */
  @PrimaryKey
  @Column
  username: string;

  /**
   * The password of the user.
   */
  @Column
  password: string;

  /**
   * An API key to represent the user.
   */
  @Column
  apiKey: string;
}
