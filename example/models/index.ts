import { Sequelize } from 'sequelize-typescript';

import { Client } from './Client.js';
import { Pet } from './Pet.js';
import { User } from './User.js';

export { Client, Pet, User };

/**
 * @param connectionURL The database URI connect to.
 */
export async function initDB(connectionURL: string): Promise<void> {
  const sequelize = new Sequelize(connectionURL, {
    define: { timestamps: false },
    logging: false,
    models: [Client, Pet, User],
  });
  await sequelize.sync();
}
