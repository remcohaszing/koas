import * as Koa from 'koa';
import * as logger from 'koa-logger';
import { bodyParser } from 'koas-body-parser';
import { koas } from 'koas-core';
import { operations } from 'koas-operations';
import { parameters } from 'koas-parameters';
import { security } from 'koas-security';
import { serializer } from 'koas-serializer';
import { specHandler } from 'koas-spec-handler';
import { statusCode } from 'koas-status-code';
import { swaggerUI } from 'koas-swagger-ui';

import { api } from './api/index.js';
import * as authentication from './authentication/index.js';
import * as controllers from './controllers/index.js';
import { initDB } from './models/index.js';
import { type Argv } from './types.js';

/**
 * Create Koa the server.
 *
 * @param argv The parsed command line arguments.
 * @returns The configured Koa app.
 */
export async function createServer({ database, secret }: Partial<Argv>): Promise<Koa> {
  const app = new Koa();
  app.keys = [secret];
  await initDB(database);
  app.context.secret = secret;
  if (process.env.NODE_ENV !== 'test') {
    app.use(logger());
  }

  app.use(
    koas(api, [
      specHandler(),
      swaggerUI(),
      security(authentication),
      statusCode(),
      serializer(),
      parameters(),
      bodyParser(),
      operations({ controllers }),
    ]),
  );
  return app;
}

/**
 * Start the server.
 *
 * @param argv The parsed command line arguments.
 */
export async function start(argv: Argv): Promise<void> {
  const { port } = argv;
  const app = await createServer(argv);
  app.listen(port, () => {
    // eslint-disable-next-line no-console
    console.log(`Listening on http://localhost:${port}`);
  });
}
