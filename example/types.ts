export interface Argv {
  /**
   * The connection string for the database connection.
   */
  database: string;

  /**
   * The port on which to start the server.
   */
  port: number;

  /**
   * The app secret.
   */
  secret: string;
}

export interface TokenResponse {
  /**
   * The access token to use for authorizing requests.
   */
  accessToken: string;
}

export interface JWTPayload {
  /**
   * The user ID of the subject.
   */
  sub: string;

  /**
   * When the token expires.
   */
  exp: number;
}

export interface PasswordCredentials {
  /**
   * The username to login with.
   */
  username: string;

  /**
   * The user entered password to login with.
   */
  password: string;
}

export interface Pet {
  /**
   * The unique identifier of the pet.
   */
  id: number;

  /**
   * The species of the pet.
   */
  species: 'cat' | 'dog';

  /**
   * The given name of the pet.
   */
  name: string;
}

declare module 'koa' {
  interface DefaultContext {
    /**
     * The app secret
     */
    secret: string;
  }
}
