module.exports = {
  collectCoverageFrom: ['packages/**/*.ts'],
  coverageReporters: ['cobertura', 'json', 'lcov', 'text'],
  moduleNameMapper: {
    [/^(koas-.*)$/.source]: '<rootDir>/packages/$1/src',
    '^(\\.{1,2}/.*)\\.js$': '$1',
  },
  reporters: ['default', 'jest-junit'],
  snapshotSerializers: ['<rootDir>/jest-string'],
  transform: {
    [/\.ts$/.source]: ['ts-jest', { isolatedModules: true }],
  },
};
