import { request, setTestApp } from 'axios-test-instance';
import * as FormData from 'form-data';
import * as Koa from 'koa';
import { koas } from 'koas-core';

import { bodyParser, File } from './index.js';

let body: unknown;

beforeEach(async () => {
  const app = new Koa();
  app.use(
    koas(
      {
        openapi: '3.0.2',
        info: {
          title: 'test',
          version: 'test',
        },
        paths: {
          '/': {
            post: {
              requestBody: {
                content: {
                  'application/json': {
                    schema: {
                      type: 'object',
                      additionalProperties: false,
                      properties: {
                        str: { type: 'string' },
                      },
                    },
                  },
                  'multipart/form-data': {
                    schema: {
                      type: 'object',
                      additionalProperties: false,
                      properties: {
                        bool: { type: 'boolean' },
                        num: { type: 'number' },
                        int: { type: 'integer' },
                        obj: { type: 'object' },
                        bools: { type: 'array', items: { type: 'boolean' } },
                        nums: { type: 'array', items: { type: 'number' } },
                        ints: { type: 'array', items: { type: 'integer' } },
                        objs: { type: 'array', items: { type: 'object' } },
                        file: { type: 'string', format: 'binary' },
                        files: { type: 'array', items: { type: 'string', format: 'binary' } },
                      },
                    },
                    encoding: {
                      file: { contentType: 'text/plain' },
                      files: { contentType: 'text/plain' },
                    },
                  },
                  'text/plain': {
                    schema: { type: 'string', minLength: 3 },
                  },
                },
              },
              responses: {
                default: {
                  description: '',
                  content: {},
                },
              },
            },
          },
          '/recursion': {
            post: {
              requestBody: {
                content: {
                  'application/json': { schema: { $ref: '#/components/schemas/Recursion' } },
                },
              },
              responses: {},
            },
          },
        },
        components: {
          schemas: {
            Recursion: {
              type: 'object',
              properties: {
                depth: { type: 'integer' },
                recursive: { $ref: '#/components/schemas/Recursion' },
              },
            },
          },
        },
      },
      [
        bodyParser(),
        () => (ctx) => {
          ({ body } = ctx.request);
          ctx.body = 'valid';
        },
      ],
    ),
  );
  await setTestApp(app);
});

afterEach(() => {
  body = undefined;
});

describe('jsonParser', () => {
  it('should transform application/json into a JavaScript object', async () => {
    const response = await request.post('/', { str: 'This is json' });
    expect(body).toStrictEqual({ str: 'This is json' });
    expect(response.data).toBe('valid');
  });

  it('should validate the application/json body', async () => {
    const response = await request.post('/', { text: 123 });
    expect(body).toBeUndefined();
    expect(response.status).toBe(400);
    expect(response.data).toStrictEqual({
      message: 'JSON schema validation failed',
      errors: [
        {
          argument: 'text',
          instance: { text: 123 },
          message: 'is not allowed to have the additional property "text"',
          name: 'additionalProperties',
          path: [],
          property: 'instance',
          schema: {
            additionalProperties: false,
            properties: { str: { type: 'string' } },
            type: 'object',
          },
          stack: 'instance is not allowed to have the additional property "text"',
        },
      ],
    });
  });

  it('should be able to handle recursive JSON schemas', async () => {
    const response = await request.post('/recursion', {
      depth: 0,
      recursive: {
        depth: 1,
        recursive: {
          depth: 2,
          recursive: {
            depth: 3,
          },
        },
      },
    });
    expect(body).toStrictEqual({
      depth: 0,
      recursive: {
        depth: 1,
        recursive: {
          depth: 2,
          recursive: {
            depth: 3,
          },
        },
      },
    });
    expect(response.data).toBe('valid');
  });
});

describe('formdataParser', () => {
  it('should transform multipart/form-data into a JavaScript object', async () => {
    const form = new FormData();
    form.append('bool', 'true');
    form.append('num', '3.14');
    form.append('int', '1');
    form.append('obj', '{}');
    form.append('bools', 'false');
    form.append('nums', '13.37');
    form.append('ints', '42');
    form.append('objs', '{}');
    const response = await request.post('/', form);
    expect(response.data).toBe('valid');
    expect(body).toStrictEqual({
      bool: true,
      bools: [false],
      int: 1,
      ints: [42],
      num: 3.14,
      nums: [13.37],
      obj: {},
      objs: [{}],
    });
  });

  it('should parse files', async () => {
    const form = new FormData();
    form.append('file', Buffer.from('Hello\n'), {
      contentType: 'text/plain',
      filename: 'hello.txt',
    });
    form.append('files', Buffer.from('Foo\n'), {
      contentType: 'text/plain',
      filename: 'foo.txt',
    });
    form.append('files', Buffer.from('Bar\n'), {
      contentType: 'text/plain',
      filename: 'bar.txt',
    });
    const response = await request.post('/', form);
    expect(response.data).toBe('valid');
    expect(body).toStrictEqual({
      file: new File(Buffer.from('Hello\n'), 'hello.txt', 'text/plain'),
      files: [
        new File(Buffer.from('Foo\n'), 'foo.txt', 'text/plain'),
        new File(Buffer.from('Bar\n'), 'bar.txt', 'text/plain'),
      ],
    });
  });

  it('should validate the multipart/form-data body', async () => {
    const form = new FormData();
    form.append('bool', 'yep');
    form.append('num', 'pi');
    form.append('int', 'one');
    form.append('obj', 'object');
    form.append('bools', 'nope');
    form.append('nums', 'leet');
    form.append('ints', 'the answer to life,the universe, and everything');
    form.append('objs', 'objects');
    const response = await request.post('/', form);
    expect(body).toBeUndefined();
    expect(response.status).toBe(400);
    expect(response.data).toStrictEqual({
      message: 'Invalid content types found',
      errors: [
        {
          argument: ['boolean'],
          instance: 'yep',
          message: 'is not of a type(s) boolean',
          name: 'type',
          path: ['bool'],
          property: 'instance.bool',
          schema: { type: 'boolean' },
          stack: 'instance.bool is not of a type(s) boolean',
        },
        {
          argument: ['number'],
          instance: null,
          message: 'is not of a type(s) number',
          name: 'type',
          path: ['num'],
          property: 'instance.num',
          schema: { type: 'number' },
          stack: 'instance.num is not of a type(s) number',
        },
        {
          argument: ['integer'],
          instance: null,
          message: 'is not of a type(s) integer',
          name: 'type',
          path: ['int'],
          property: 'instance.int',
          schema: { type: 'integer' },
          stack: 'instance.int is not of a type(s) integer',
        },
        {
          argument: ['object'],
          instance: 'object',
          message: 'is not of a type(s) object',
          name: 'type',
          path: ['obj'],
          property: 'instance.obj',
          schema: { type: 'object' },
          stack: 'instance.obj is not of a type(s) object',
        },
        {
          argument: ['boolean'],
          instance: 'nope',
          message: 'is not of a type(s) boolean',
          name: 'type',
          path: ['bools', 0],
          property: 'instance.bools[0]',
          schema: { type: 'boolean' },
          stack: 'instance.bools[0] is not of a type(s) boolean',
        },
        {
          argument: ['number'],
          instance: null,
          message: 'is not of a type(s) number',
          name: 'type',
          path: ['nums', 0],
          property: 'instance.nums[0]',
          schema: { type: 'number' },
          stack: 'instance.nums[0] is not of a type(s) number',
        },
        {
          argument: ['integer'],
          instance: null,
          message: 'is not of a type(s) integer',
          name: 'type',
          path: ['ints', 0],
          property: 'instance.ints[0]',
          schema: { type: 'integer' },
          stack: 'instance.ints[0] is not of a type(s) integer',
        },
        {
          argument: ['object'],
          instance: 'objects',
          message: 'is not of a type(s) object',
          name: 'type',
          path: ['objs', 0],
          property: 'instance.objs[0]',
          schema: { type: 'object' },
          stack: 'instance.objs[0] is not of a type(s) object',
        },
      ],
    });
  });

  it('should validate files based on the content type', async () => {
    const form = new FormData();
    form.append('file', Buffer.from('{}'), {
      contentType: 'application/json',
      filename: 'hello.json',
    });
    form.append('files', Buffer.from('{}'), {
      contentType: 'application/json',
      filename: 'foo.json',
    });
    form.append('files', Buffer.from('{}'), {
      contentType: 'application/json',
      filename: 'bar.json',
    });
    const response = await request.post('/', form);
    expect(body).toBeUndefined();
    expect(response.status).toBe(400);
    expect(response.data).toStrictEqual({
      message: 'Invalid content types found',
      errors: [
        {
          argument: 'text/plain',
          instance: '{}',
          message: 'has an invalid content type',
          name: 'contentType',
          path: ['file'],
          property: 'instance.file',
          schema: {},
          stack: 'instance has an invalid content type',
        },
        {
          argument: 'text/plain',
          instance: '{}',
          message: 'has an invalid content type',
          name: 'contentType',
          path: ['files', 0],
          property: 'instance.files[0]',
          schema: {},
          stack: 'instance has an invalid content type',
        },
        {
          argument: 'text/plain',
          instance: '{}',
          message: 'has an invalid content type',
          name: 'contentType',
          path: ['files', 1],
          property: 'instance.files[1]',
          schema: {},
          stack: 'instance has an invalid content type',
        },
      ],
    });
  });
});

describe('textParser', () => {
  it('should transform text/plain into a regular string', async () => {
    const response = await request.post('/', '{"text":"This is plain text"}', {
      headers: { 'content-type': 'text/plain' },
    });
    expect(response.data).toBe('valid');
    expect(body).toBe('{"text":"This is plain text"}');
  });

  it('should validate the text/plain body', async () => {
    const response = await request.post('/', '', {
      headers: { 'content-type': 'text/plain' },
    });
    expect(body).toBeUndefined();
    expect(response.status).toBe(400);
    expect(response.data).toStrictEqual({
      message: 'JSON schema validation failed',
      errors: [
        {
          argument: 3,
          instance: '',
          message: 'does not meet minimum length of 3',
          name: 'minLength',
          path: [],
          property: 'instance',
          schema: { type: 'string', minLength: 3 },
          stack: 'instance does not meet minimum length of 3',
        },
      ],
    });
  });
});
