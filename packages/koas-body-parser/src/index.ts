import { type Readable } from 'node:stream';

import * as inflate from 'inflation';
import { type Context } from 'koa';
import { type Plugin, type PluginOptions } from 'koas-core';
import { type OpenAPIV3 } from 'openapi-types';
import { is } from 'type-is';

import { File, formdataParser } from './parsers/formdata.js';
import { jsonParser } from './parsers/json.js';
import { textParser } from './parsers/text.js';

export { bufferParser } from './parsers/buffer.js';

export { File, formdataParser, jsonParser, textParser };

export interface Parser<T> {
  (
    body: Readable,
    mediaTypeObject: OpenAPIV3.MediaTypeObject,
    options: PluginOptions,
    ctx: Context,
  ): Promise<T>;

  /**
   * Set this to make the parser output bypass JSON schema validation.
   *
   * @default false
   */
  skipValidation?: boolean;
}

declare module 'koa' {
  interface Request {
    /**
     * The request body that is parsed by `koas-body-parser`.
     */
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    body: any;
  }
}

export interface BodyParserParams {
  /**
   * A mapping of body parsers.
   *
   * The keys will be matched with the `Content-Type` header. The best matching parser will be used
   * to parse the body.
   */
  parsers?: Record<string, Parser<unknown>>;

  /**
   * A list of operation IDs for which to skip parsing the body.
   */
  ignore?: string[];
}

/**
 * Authomatically parse a request body based on the JSON schema for the operation.
 *
 * @param options Options for the request body parser.
 * @returns Koas middleware which automatically parses the request body.
 */
export function bodyParser({ ignore = [], parsers = {} }: BodyParserParams = {}): Plugin {
  const allParsers: Record<string, Parser<unknown>> = {
    'application/json': jsonParser,
    'multipart/form-data': formdataParser,
    'text/plain': textParser,
    ...parsers,
  };

  const supportedMimeTypes = Object.keys(allParsers).sort((a, b) => (a > b ? -1 : 1));

  return (options) => {
    const { resolveRef, validate } = options;
    return async (ctx: Context, next) => {
      const { operationObject } = ctx.openApi;
      if (ignore.includes(operationObject.operationId)) {
        return next();
      }
      if (!operationObject.requestBody) {
        return next();
      }
      const { content, required } = resolveRef(operationObject.requestBody);
      const requestContentType = ctx.request.type || 'application/octet-stream';
      ctx.assert(is(requestContentType, Object.keys(content)), 415);
      const type = supportedMimeTypes.find((mime) => is(requestContentType, mime));
      const parser = allParsers[type];
      ctx.assert(parser, 415);
      const mediaTypeObject = content[type];
      const body = await parser(inflate(ctx.req), mediaTypeObject, options, ctx);
      ctx.assert(!required || body !== undefined, 400, 'Missing request body');
      if (!parser.skipValidation && mediaTypeObject && mediaTypeObject.schema) {
        validate(body, mediaTypeObject.schema);
      }
      ctx.request.body = body;
      return next();
    };
  };
}
