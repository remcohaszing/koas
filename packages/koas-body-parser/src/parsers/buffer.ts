import * as raw from 'raw-body';

import { type Parser } from '../index.js';

/**
 * Parse the request body into a buffer.
 *
 * @param body The request body to parse.
 * @param mediaTypeObject Unused.
 * @param options The koas plugin options.
 * @param ctx The Koa context. This will be used to determinethe body length to expect.
 * @returns The parsed request body.
 */
export const bufferParser: Parser<Buffer> = (body, mediaTypeObject, options, ctx) =>
  raw(body, {
    length: ctx.request.length,
  });
