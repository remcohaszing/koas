import * as busboy from 'busboy';
import { SchemaValidationError } from 'koas-core';
import { type OpenAPIV3 } from 'openapi-types';
import { is } from 'type-is';

import { type Parser } from '../index.js';

/**
 * Parse a raw string value based on a JSON schema.
 *
 * @param schema The JSON schema whose type to use for parsing the value.
 * @param content The value to parse.
 * @returns The parsed value.
 */
// eslint-disable-next-line @typescript-eslint/no-explicit-any
function fromString(schema: OpenAPIV3.SchemaObject, content: string): any {
  switch (schema?.type) {
    case 'integer':
    case 'number':
      return Number(content);
    case 'boolean':
      if (content === 'true') {
        return true;
      }
      if (content === 'false') {
        return false;
      }
      return content;
    case 'object':
      try {
        return JSON.parse(content);
      } catch {
        return content;
      }
    default:
      return content;
  }
}

/**
 * An representation of an incoming file from a form-data request.
 */
export class File {
  /**
   * The contents of the file field
   */
  contents: Buffer;

  /**
   * The filename of the file field.
   */
  filename: string;

  /**
   * The mime type of the file field.
   */
  mime: string;

  constructor(contents: Buffer, filename: string, mime?: string) {
    this.contents = contents;
    this.filename = filename;
    this.mime = mime;
  }
}

/**
 * Parse the `multipar/form-data` request body based on the JSON schema.
 *
 * Files will be transformed into `VFile` objects. all other types will be converted to their
 * respective types based on the JSON schema.
 *
 * @param body The request body to parse.
 * @param mediaTypeObject The media object used to determine how to parse the body.
 * @param options The koas plugin options.
 * @param ctx The current Koa context.
 * @returns The request body parsed into an object.
 */
export const formdataParser: Parser<Record<string, unknown>> = async (
  body,
  mediaTypeObject,
  { resolveRef },
  ctx,
) => {
  const bb = busboy({ headers: ctx.req.headers });
  const { encoding = {}, schema } = mediaTypeObject || {};
  const { properties = {} } = resolveRef(schema) || {};
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const response: Record<string, any> = {};
  const buffers: Record<string, File | File[]> = {};

  await new Promise((resolve, reject) => {
    /**
     * Handle an erroremitted by busboy.
     *
     * @param error The error that was emitted.
     */
    function onError(error: Error): void {
      bb.removeAllListeners();
      reject(error);
    }

    bb.on('file', (fieldname, stream, { filename, mimeType }) => {
      const buffer: Buffer[] = [];
      stream.on('data', (data) => {
        buffer.push(data);
      });
      stream.on('end', () => {
        const data = new File(Buffer.concat(buffer), filename, mimeType);
        const propertySchema = resolveRef(properties[fieldname]);
        if (propertySchema && propertySchema.type === 'array') {
          if (!Object.hasOwnProperty.call(response, fieldname)) {
            response[fieldname] = [];
          }
          if (!Object.hasOwnProperty.call(buffers, fieldname)) {
            buffers[fieldname] = [];
          }
          (response[fieldname] as string[]).push('');
          (buffers[fieldname] as File[]).push(data);
        } else {
          response[fieldname] = '';
          buffers[fieldname] = data;
        }
      });
    })
      .on('field', (fieldname, content) => {
        const propertySchema = resolveRef(properties[fieldname]);
        if (propertySchema && propertySchema.type === 'array') {
          if (!Object.hasOwnProperty.call(response, fieldname)) {
            response[fieldname] = [];
          }
          (response[fieldname] as string[]).push(
            fromString(resolveRef(propertySchema.items), content),
          );
        } else {
          response[fieldname] = fromString(propertySchema, content);
        }
      })
      .on('close', () => {
        bb.removeAllListeners();
        resolve(response);
      })
      .on('error', onError)
      .on('partsLimit', onError)
      .on('filesLimit', onError)
      .on('fieldsLimit', onError);
    ctx.req.pipe(bb);
  });

  const result = ctx.openApi.validate(response, schema, { throw: false });
  for (const [key, values] of Object.entries(buffers)) {
    const vals = Array.isArray(values) ? values : [values];
    for (const [index, value] of vals.entries()) {
      if (!encoding?.[key]?.contentType) {
        continue;
      }
      if (
        !is(
          value.mime,
          encoding[key].contentType.split(',').map((contentType) => contentType.trim()),
        )
      ) {
        const error = result.addError({
          name: 'contentType',
          argument: encoding[key].contentType,
          message: 'has an invalid content type',
        });
        error.schema = {};
        error.path = values === value ? [key] : [key, index];
        error.property = values === value ? `instance.${key}` : `instance.${key}[${index}]`;
        error.instance =
          value.contents.length < 30
            ? String(value.contents)
            : `${value.contents.slice(0, 5)}...[TRUNCATED]...${value.contents.slice(-5)}`;
      }
    }
  }
  if (!result.valid) {
    throw new SchemaValidationError('Invalid content types found', { result });
  }
  return Object.assign(response, buffers);
};

formdataParser.skipValidation = true;
