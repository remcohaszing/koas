import { textParser } from './text.js';
import { type Parser } from '../index.js';

/**
 * Parse a JSON body into an object
 *
 * @param body The request body to parse.
 * @param mediaTypeObject Unused
 * @param options The koas plugin options.
 * @param ctx The current Koa context.
 * @returns The request body as an object.
 */
export const jsonParser: Parser<unknown> = (body, mediaTypeObject, options, ctx) =>
  textParser(body, mediaTypeObject, options, ctx).then(JSON.parse);
