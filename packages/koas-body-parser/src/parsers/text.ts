import * as raw from 'raw-body';

import { type Parser } from '../index.js';

/**
 * Parse the request body into a plain text string.
 *
 * @param body The request body to parse.
 * @param mediaTypeObject Unused.
 * @param options The koas plugin options.
 * @param ctx The Koa context. This will be used to determine the charset to use and the body
 *   length to expect.
 * @returns The parsed request body.
 */
export const textParser: Parser<string> = (body, mediaTypeObject, options, ctx) =>
  raw(body, {
    encoding: ctx.request.charset || 'utf8',
    length: ctx.request.length,
  });
