# Koas-core

> [Koa][] + [OpenAPI Specification][] = Koas

Koas aims to make it easy to write a RESTful API based on Koa and an Open API V3 Specification.

This is the core package. For more details, see the
[mono repository README](https://gitlab.com/remcohaszing/koas)

## Installation

```sh
npm install koa koas-core
```

## Usage

```js
const Koa = require('koa');
const { koas } = require('koas-core');

const document = {
  openapi: '3.0.2',
  info: {
    title: 'My API',
    version: '1.0.0',
  },
  paths: {
    '/': {
      get: {
        responses: {},
      },
    },
  },
};

const app = new Koa();
app.use(
  koas(document, [
    // Koas plugins go here.
  ]),
);
app.listen(3333);
```

## Plugins

Plugins are functions that take parameters provided by Koas, and return a Koa middleware. Typically,
plugins are returned by a function, so options can be passed in.

Example plugin:

```js
function myPlugin(options) {
  return (document, resolveRef, runAlways, validate) => async (ctx, next) => {
    await next();
  };
}

module.exports = {
  myPlugin,
};
```

TypeScript typings are provided. So it is also possible to write plugins using TypeScript. Pretty
much the only type annotation needed is `Plugin` as the return option for the function.

```ts
import { type Plugin } from 'koas-core';

export interface MyPluginOptions {
  myOption: unknown;
}

export function myPlugin(options: MyPluginOptions): Plugin {
  return (document, resolveRef, runAlways, validate) => async (ctx, next) => {
    await next();
  };
}
```

[koa]: https://koajs.com
[openapi specification]: https://spec.openapis.org/oas/v3.0.3
