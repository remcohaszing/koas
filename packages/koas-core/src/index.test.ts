import { request, setTestApp } from 'axios-test-instance';
import * as Koa from 'koa';
import { type OpenAPIV3 } from 'openapi-types';

import { koas } from './index.js';

/**
 * Koa middleware that does nothing.
 *
 * @param ctx Unused
 * @param next The Koa next function.
 */
async function noop(ctx: Koa.Context, next: Koa.Next): Promise<void> {
  await next();
}

const document: OpenAPIV3.Document = {
  openapi: '3.0.2',
  info: {
    title: 'Test server',
    version: 'test',
  },
  components: {
    schemas: {
      foo: { type: 'string' },
      bar: { $ref: '#/components/schemas/foo' },
    },
  },
  paths: {
    '/': {
      get: {
        responses: { 200: { description: '' } },
      },
    },
  },
};

let context: Koa.Context;
let injectedArgs: unknown;
let middleware: Koa.Middleware;
let middlewares: jest.Mock<Promise<unknown>>[];

beforeEach(async () => {
  middlewares = [jest.fn(noop), jest.fn(noop)];
  const app = new Koa();
  app.use(
    koas(document, [
      (args) => async (ctx, next) => {
        injectedArgs = args;
        context = ctx;
        if (middleware) {
          await middleware(ctx, next);
        }
        await next();
      },
      ...middlewares.map((m) => () => m),
    ]),
  );
  await setTestApp(app);
});

describe('koas', () => {
  it('should call middleware', async () => {
    await request.get('/');
    expect(middlewares[0]).toHaveBeenCalledTimes(1);
    expect(middlewares[0]).toHaveBeenCalledTimes(1);
  });

  it('should inject the raw and dereferenced OpenAPI document', async () => {
    await request.get('/');
    expect(injectedArgs).toMatchObject({
      document: {
        openapi: '3.0.2',
        info: {
          title: 'Test server',
          version: 'test',
        },
        components: {
          schemas: {
            foo: { type: 'string' },
            bar: { $ref: '#/components/schemas/foo' },
          },
        },
        paths: {
          '/': {
            get: {
              responses: {},
            },
          },
        },
      },
      runAlways: expect.any(Function),
    });
  });

  it('should configure openApi related fields for middleware', async () => {
    await request.get('/');
    expect(context.openApi.pathItemObject).toStrictEqual(document.paths['/']);
    expect(context.openApi.operationObject).toStrictEqual(document.paths['/'].get);
  });

  it('should handle JSON schema validation errors gracefully if throwAll is used', async () => {
    middleware = (ctx) => {
      ctx.openApi.validate('', { type: 'object' });
    };
    const response = await request.get('/');
    expect(response.status).toBe(400);
    expect(response.data).toStrictEqual({
      message: 'JSON schema validation failed',
      errors: [
        {
          argument: ['object'],
          instance: '',
          message: 'is not of a type(s) object',
          name: 'type',
          path: [],
          property: 'instance',
          schema: { type: 'object' },
          stack: 'instance is not of a type(s) object',
        },
      ],
    });
  });

  it('should handle JSON schema validation errors gracefully if throwFirst is used', async () => {
    middleware = (ctx) => {
      ctx.openApi.validate('', { type: 'object' });
    };
    const response = await request.get('/');
    expect(response.status).toBe(400);
    expect(response.data).toStrictEqual({
      message: 'JSON schema validation failed',
      errors: [
        {
          argument: ['object'],
          instance: '',
          message: 'is not of a type(s) object',
          name: 'type',
          path: [],
          property: 'instance',
          schema: { type: 'object' },
          stack: 'instance is not of a type(s) object',
        },
      ],
    });
  });
});
