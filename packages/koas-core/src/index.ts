import {
  type PreValidatePropertyFunction,
  type RewriteFunction,
  type ValidatorResult,
} from 'jsonschema';
import { type Context, type Middleware } from 'koa';
import * as compose from 'koa-compose';
import { type OpenAPIV3 } from 'openapi-types';

import { createResolver, JSONRefResolver } from './jsonRefs.js';
import { createMatcher } from './matcher.js';
import { createValidator, SchemaValidationError } from './validation.js';

export { JSONRefResolver, SchemaValidationError };

export interface KoasOptions {
  /**
   * Convert a schema validation error to an HTTP response body.
   *
   * @param error The error that as thrown.
   * @param ctx The Koa context.
   * @returns The HTTP response body.
   */
  onSchemaValidationError?: (error: SchemaValidationError, ctx: Context) => unknown;
}

interface ValidateOptions {
  /**
   * The error message.
   *
   * @default 'JSON schema validation failed'
   */
  message?: string;

  /**
   * A function to rewrite a property before it’s passed to the JSON schema validation
   */
  preValidateProperty?: PreValidatePropertyFunction;

  /**
   * A function to rewrite a property after has passed JSON schema validation
   */
  rewrite?: RewriteFunction;

  /**
   * The HTTP status code to set.
   *
   * @default 400
   */
  status?: number;

  /**
   * Whether or not to throw an error if the schema validation failed.
   *
   * If `false`, the `ValidatorResult` instance will be returned instead.
   *
   * @default true
   */
  throw?: boolean;
}

/**
 * A JSON schema validator function.
 *
 * @param instance The instance to validate
 * @param schema The JSON schema to use.
 * @param options Additional jsonschema validation options.
 * @returns The validator result
 */
export type Validator = (
  instance: unknown,
  schema: OpenAPIV3.ReferenceObject | OpenAPIV3.SchemaObject,
  options?: ValidateOptions,
) => ValidatorResult;

declare module 'koa' {
  interface DefaultContext {
    /**
     * The OpenAPI specitic context injected by koas.
     */
    openApi?: OpenAPIContext;
  }
}

export interface OpenAPIContext {
  /**
   * The full OpenAPI document
   */
  document: OpenAPIV3.Document;

  /**
   * The OpenAPI operation object that describes the current request context.
   */
  operationObject?: OpenAPIV3.OperationObject;

  /**
   * The path item object that describes the current request context.
   */
  pathItemObject?: OpenAPIV3.PathItemObject;

  /**
   * A function to apply JSON schema validation in the context of the OpenAPI document.
   */
  validate: Validator;
}

/**
 * A function that takes Koas options and returns Koa Middleware.
 */
export type Plugin = (options: PluginOptions) => Middleware;

export interface PluginOptions {
  /**
   * The OpenAPI document that was given to Koas.
   */
  document: OpenAPIV3.Document;

  /**
   * Resolve a JSON pointer reference in the OpenAPI document.
   */
  resolveRef: JSONRefResolver;

  /**
   * Mark a middlere to it always runs, even if there is no matching route.
   */
  runAlways: (middleware: Middleware) => Middleware;

  /**
   * Validate an object using a JSON schema.
   *
   * References will be resolved against the OpenAPI document.
   */
  validate: Validator;
}

const methods = new Set([
  'delete',
  'get',
  'head',
  'options',
  'patch',
  'post',
  'put',
  'trace',
] as const);

/**
 * This symbol is used internally to specify a middleware should always be run.
 */
const RUN_ALWAYS = Symbol('This middleware is always run');

/**
 * Mark that middleware always needs to run, even if there is no matching OpenAPI operation.
 *
 * @param middleware The middleware to mark.
 * @returns The marked middleware itself.
 */
function markRunAlways(middleware: Middleware): Middleware {
  // @ts-expect-error This is an internal hack.
  // eslint-disable-next-line no-param-reassign
  middleware[RUN_ALWAYS] = true;
  return middleware;
}

/**
 * Create a Koa middleware from Koas middlewares.
 *
 * @param document The OpenAPI document from which to create an API.
 * @param middlewares The Koas middlewares to use for creating an API.
 * @param options Advanced options
 * @returns Koa middleware that processes requests according the the OpenAPI document.
 */
export function koas(
  document: OpenAPIV3.Document,
  middlewares: Plugin[] = [],
  {
    onSchemaValidationError = (error) => ({ message: error.message, errors: error.result.errors }),
  }: KoasOptions = {},
): Middleware {
  const resolveRef = createResolver(document);
  const matchers = Object.entries(document.paths).map(([pathTemplate, pathItemObject]) => {
    const matcher = createMatcher(pathTemplate, resolveRef, pathItemObject.parameters);
    return [matcher, pathItemObject] as const;
  });
  const validator = createValidator(document);
  const validate: Validator = (
    instance,
    schema,
    {
      message = 'JSON schema validation failed',
      preValidateProperty,
      rewrite,
      status,
      throw: throwError = true,
    } = {},
  ) => {
    const result = validator.validate(instance, schema, {
      base: '#',
      nestedErrors: true,
      rewrite,
      preValidateProperty,
    });
    if (throwError && !result.valid) {
      throw new SchemaValidationError(message, { result, status });
    }
    return result;
  };
  const pluginOptions: PluginOptions = {
    document,
    resolveRef,
    runAlways: markRunAlways,
    validate,
  };

  const injected = middlewares.map((middleware) => middleware(pluginOptions));
  const composed = compose(injected);
  // @ts-expect-error This is an internal hack.
  const runAlways = compose(injected.filter((middleware) => middleware[RUN_ALWAYS]));

  return async (ctx, next) => {
    let params: Record<string, string>;
    const match = matchers.find(([matcher]) => {
      params = matcher(ctx.path);
      return Boolean(params);
    });
    ctx.openApi = { document, validate };
    try {
      if (!match) {
        return await runAlways(ctx, next);
      }
      const [, pathItemObject] = match;
      ctx.openApi.pathItemObject = pathItemObject;
      ctx.params = params;
      const method = ctx.method.toLowerCase();
      if (!methods.has(method as 'get')) {
        return await runAlways(ctx, next);
      }
      const operationObject = pathItemObject[method as 'get'];
      if (!operationObject) {
        return await runAlways(ctx, next);
      }
      ctx.openApi.operationObject = operationObject;
      await composed(ctx, next);
    } catch (error: unknown) {
      if (error instanceof SchemaValidationError) {
        ctx.status = error.status;
        ctx.body = onSchemaValidationError(error, ctx);
        return;
      }
      throw error;
    }
  };
}
