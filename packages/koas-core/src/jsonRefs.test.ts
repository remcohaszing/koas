import { createResolver, escapeJsonPointer, unescapeJsonPointer } from './jsonRefs.js';

const tests = [
  ['foo/bar', 'foo~1bar'],
  ['foo~bar', 'foo~0bar'],
  ['foo~/bar', 'foo~0~1bar'],
  ['foo/~bar', 'foo~1~0bar'],
];

describe('escapeJsonPointer', () => {
  it.each(tests)('%s → %s', (unescaped, escaped) => {
    const result = escapeJsonPointer(unescaped);
    expect(result).toBe(escaped);
  });
});

describe('unescapeJsonPointer', () => {
  it.each(tests)('%s ← %s', (unescaped, escaped) => {
    const result = unescapeJsonPointer(escaped);
    expect(result).toBe(unescaped);
  });
});

describe('createResolver', () => {
  it('should handle null', () => {
    const resolveRef = createResolver({
      openapi: '3.0.3',
      info: { title: 'Title', version: '0.0.0' },
      paths: {},
    });
    const result = resolveRef(null);
    expect(result).toBeNull();
  });

  it('should handle non-objects', () => {
    const resolveRef = createResolver({
      openapi: '3.0.3',
      info: { title: 'Title', version: '0.0.0' },
      paths: {},
    });
    const result = resolveRef(123);
    expect(result).toBe(123);
  });

  it('should handle objects that aren’t JSON references', () => {
    const resolveRef = createResolver({
      openapi: '3.0.3',
      info: { title: 'Title', version: '0.0.0' },
      paths: {},
    });
    const result = resolveRef({ foo: 'bar' });
    expect(result).toStrictEqual({ foo: 'bar' });
  });

  it('should handle invalid $ref types', () => {
    const resolveRef = createResolver({
      openapi: '3.0.3',
      info: { title: 'Title', version: '0.0.0' },
      paths: {},
    });
    expect(() => resolveRef({ $ref: 1 })).toThrow(
      new TypeError('Invalid JSON pointer. Expected a string. Got: 1'),
    );
  });

  it('should handle invalid pointers', () => {
    const resolveRef = createResolver({
      openapi: '3.0.3',
      info: { title: 'Title', version: '0.0.0' },
      paths: {},
    });
    expect(() => resolveRef({ $ref: '/info' })).toThrow(
      new TypeError("Invalid JSON pointer. It should start with '#/'. Got: '/info'"),
    );
  });

  it('should handle unresolved object properties', () => {
    const resolveRef = createResolver({
      openapi: '3.0.3',
      info: { title: 'Title', version: '0.0.0' },
      paths: {},
    });
    expect(() => resolveRef({ $ref: '#/info/foo' })).toThrow(
      new TypeError("Unresolved JSON pointer: '#/info/foo'. Property 'foo' could not be resolved."),
    );
  });

  it('should handle unresolved non-object properties', () => {
    const resolveRef = createResolver({
      openapi: '3.0.3',
      info: { title: 'Title', version: '0.0.0' },
      paths: {},
    });
    expect(() => resolveRef({ $ref: '#/info/title/foo' })).toThrow(
      new TypeError(
        "Unresolved JSON pointer: '#/info/title/foo'. Property 'foo' could not be resolved.",
      ),
    );
  });

  it('should handle unresolved null properties', () => {
    const resolveRef = createResolver(null);
    expect(() => resolveRef({ $ref: '#/info/title/foo' })).toThrow(
      new TypeError(
        "Unresolved JSON pointer: '#/info/title/foo'. Property 'info' could not be resolved.",
      ),
    );
  });

  it('should handle circular references', () => {
    const resolveRef = createResolver({
      openapi: '3.0.3',
      info: { title: 'Title', version: '0.0.0' },
      paths: {},
      components: {
        schemas: {
          foo: { $ref: '#/components/schemas/bar' },
          bar: { $ref: '#/components/schemas/foo' },
        },
      },
    });
    expect(() => resolveRef({ $ref: '#/components/schemas/foo' })).toThrow(
      new Error(
        "Circular JSON reference found: '#/components/schemas/foo' → '#/components/schemas/bar' → #/components/schemas/foo",
      ),
    );
  });

  it('should resolve JSON references', () => {
    const resolveRef = createResolver({
      openapi: '3.0.3',
      info: { title: 'Title', version: '0.0.0' },
      paths: {},
    });
    const result = resolveRef({ $ref: '#/openapi' });
    expect(result).toBe('3.0.3');
  });

  it('should resolve deep JSON references', () => {
    const resolveRef = createResolver({
      openapi: '3.0.3',
      info: { title: 'Title', version: '0.0.0' },
      paths: {},
    });
    const result = resolveRef({ $ref: '#/info/title' });
    expect(result).toBe('Title');
  });

  it('should resolve escaped JSON references', () => {
    const resolveRef = createResolver({
      openapi: '3.0.3',
      info: { title: 'Title', version: '0.0.0' },
      paths: {
        '/foo/bar': { description: 'Description' },
      },
    });
    const result = resolveRef({ $ref: '#/paths/~1foo~1bar/description' });
    expect(result).toBe('Description');
  });

  it('should resolve escaped JSON references cyclically', () => {
    const resolveRef = createResolver({
      openapi: '3.0.3',
      info: { title: 'Title', version: '0.0.0' },
      paths: {},
      components: {
        schemas: {
          foo: { type: 'string' },
          bar: { $ref: '#/components/schemas/foo' },
        },
      },
    });
    const result = resolveRef({ $ref: '#/components/schemas/bar' });
    expect(result).toStrictEqual({ type: 'string' });
  });
});
