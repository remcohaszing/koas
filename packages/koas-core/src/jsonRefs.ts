import { inspect } from 'node:util';

import { type OpenAPIV3 } from 'openapi-types';

/**
 * A function to resolve a JSON reference in a given document.
 *
 * @param ref The JSON reference object to resolve
 * @returns The resolved value.
 */
export type JSONRefResolver = <T>(ref: OpenAPIV3.ReferenceObject | T) => T;

/**
 * Escape a JSON pointer segment.
 *
 * See https://tools.ietf.org/html/rfc6901#section-3
 *
 * @param pointer THe JSON pointer segment to escape.
 * @returns The escaped JSON pointer segment.
 */
export function escapeJsonPointer(pointer: string): string {
  return pointer.replace(/~/g, '~0').replace(/\//g, '~1');
}

/**
 * Unescape a JSON pointer segment.
 *
 * See https://tools.ietf.org/html/rfc6901#section-3
 *
 * @param pointer The JSON pointer segment to unescape
 * @returns The unescaped JSON pointer segment.
 */
export function unescapeJsonPointer(pointer: string): string {
  return pointer.replace(/~1/g, '/').replace(/~0/g, '~');
}

/**
 * Create a function to resolve a JSON pointer from an OpenAPI document with caching.
 *
 * @param document The OpenAPI document to create a resolver for.
 * @returns A JSON ref resolver.
 */
export function createResolver(document: OpenAPIV3.Document): JSONRefResolver {
  const cache = new Map<string, unknown>();

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const resolver = (ref: any, stack: string[]): any => {
    // Null, undefined
    if (!ref) {
      return ref;
    }
    // Primitives
    if (typeof ref !== 'object') {
      return ref;
    }
    // Non reference objects
    if (!('$ref' in ref)) {
      return ref;
    }

    const { $ref } = ref;
    if (typeof $ref !== 'string') {
      throw new TypeError(`Invalid JSON pointer. Expected a string. Got: ${inspect($ref)}`);
    }
    if (stack.includes($ref)) {
      throw new Error(
        `Circular JSON reference found: ${stack
          .map((frame) => inspect(frame))
          .concat($ref)
          .join(' → ')}`,
      );
    }

    if (cache.has($ref)) {
      return cache.get($ref);
    }

    const [hashtag, ...segments] = $ref.split('/').map(unescapeJsonPointer);
    if (hashtag !== '#') {
      throw new TypeError(`Invalid JSON pointer. It should start with '#/'. Got: ${inspect($ref)}`);
    }
    // eslint-disable-next-line @typescript-eslint/no-explicit-any, unicorn/no-array-reduce
    const result = segments.reduce<any>((acc, name) => {
      if (acc && typeof acc === 'object' && name in acc) {
        return acc[name];
      }
      throw new Error(
        `Unresolved JSON pointer: ${inspect($ref)}. Property ${inspect(
          name,
        )} could not be resolved.`,
      );
    }, document);

    const dereferenced = resolver(result, [...stack, $ref]);
    cache.set($ref, dereferenced);
    return dereferenced;
  };

  return (ref) => resolver(ref, []);
}
