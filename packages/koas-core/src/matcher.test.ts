import { createResolver } from './jsonRefs.js';
import { createMatcher } from './matcher.js';

describe('createMatcher', () => {
  it.each([
    ['/', '/', {}],
    ['/{foo}', '/bar', { foo: 'bar' }],
    ['/a/{a}/b/{b}/c/{c}', '/a/x/b/y/c/z', { a: 'x', b: 'y', c: 'z' }],
    ['/', '/todeep', null],
    ['/{foo}', '/', null],
  ])('%s should transform %s into %o', (template, url, params) => {
    const matcher = createMatcher(
      template,
      createResolver({ openapi: '3.0.3', info: { title: '', version: '' }, paths: {} }),
    );
    const result = matcher(url);
    // Test twice, because some RegExp functions hold a global state.
    expect(result).toStrictEqual(params);
    expect(result).toStrictEqual(params);
  });
});
