import { type OpenAPIV3 } from 'openapi-types';

import { type JSONRefResolver } from './jsonRefs.js';

/**
 * A matcher function for extracting URL parameters.
 *
 * @param url The URL path to process.
 * @returns A mapping of parameter names to their extracted values.
 */
export type MatcherFunction = (url: string) => Record<string, string>;

/**
 * Create a matcher function for an OpenAPI path template.
 *
 * @param pathTemplate The OpenAPI path template for which to create a matcher.
 * @param resolveRef A JSON reference resolver.
 * @param pathParameters The OpenAPI path parameter objects used to determine how to process the
 *   extracted parameters.
 * @returns a matcher function for extracting path parameters from a URL path.
 */
export function createMatcher(
  pathTemplate: string,
  resolveRef: JSONRefResolver,
  pathParameters: (OpenAPIV3.ParameterObject | OpenAPIV3.ReferenceObject)[] = [],
): MatcherFunction {
  const arrayNames = new Set(
    pathParameters
      .map(resolveRef)
      .filter(({ schema = {} }) => resolveRef(schema).type === 'array')
      .map(({ name }) => name),
  );
  const names: string[] = [];
  const pathRegex = new RegExp(
    `^${pathTemplate.replace(/{(\w+)}/gi, (match, name) => {
      names.push(name);
      return arrayNames.has(name) ? '(.*)' : '([^/]+)';
    })}$`,
    'i',
  );

  return (url) => {
    const urlMatch = url.match(pathRegex);
    if (!urlMatch) {
      return null;
    }
    const result: Record<string, string> = {};
    for (const [index, name] of names.entries()) {
      result[name] = decodeURI(urlMatch[index + 1]);
    }
    return result;
  };
}
