import { createValidator } from './validation.js';

describe('createValidator', () => {
  it('should register all schema components', () => {
    const validator = createValidator({
      openapi: '3.0.2',
      info: { title: '', version: '' },
      paths: {},
      components: {
        schemas: {
          test: { type: 'object', properties: { foo: { type: 'string' } } },
          test2: { type: 'string' },
        },
      },
    });
    expect(validator.schemas).toStrictEqual({
      '#/components/schemas/test': { properties: { foo: { type: 'string' } }, type: 'object' },
      '#/components/schemas/test/properties/foo': { type: 'string' },
      '#/components/schemas/test2': { type: 'string' },
    });
  });

  it('should register all header components', () => {
    const validator = createValidator({
      openapi: '3.0.2',
      info: { title: '', version: '' },
      paths: {},
      components: {
        headers: {
          foo: { schema: { type: 'string' } },
          bar: { schema: { type: 'number' } },
        },
      },
    });
    expect(validator.schemas).toStrictEqual({
      '#/components/headers/bar/schema': { type: 'number' },
      '#/components/headers/foo/schema': { type: 'string' },
    });
  });

  it('should register all parameter components', () => {
    const validator = createValidator({
      openapi: '3.0.2',
      info: { title: '', version: '' },
      paths: {},
      components: {
        parameters: {
          foo: { name: '', in: '', schema: { type: 'string' } },
          bar: { name: '', in: '', schema: { type: 'number' } },
        },
      },
    });
    expect(validator.schemas).toStrictEqual({
      '#/components/parameters/bar/schema': { type: 'number' },
      '#/components/parameters/foo/schema': { type: 'string' },
    });
  });

  it('should register all request body components', () => {
    const validator = createValidator({
      openapi: '3.0.2',
      info: { title: '', version: '' },
      paths: {},
      components: {
        requestBodies: {
          foo: { content: { 'application/json': { schema: { type: 'string' } } } },
          bar: { content: { 'application/json': { schema: { type: 'number' } } } },
        },
      },
    });
    expect(validator.schemas).toStrictEqual({
      '#/components/requestBodies/bar/content/application~1json/schema': { type: 'number' },
      '#/components/requestBodies/foo/content/application~1json/schema': { type: 'string' },
    });
  });

  it('should register all response components', () => {
    const validator = createValidator({
      openapi: '3.0.2',
      info: { title: '', version: '' },
      paths: {},
      components: {
        responses: {
          foo: {
            description: '',
            content: { 'application/json': { schema: { type: 'string' } } },
            headers: { 'x-foo': { schema: { type: 'number' } } },
          },
          bar: {
            description: '',
            content: { 'application/json': { schema: { type: 'number' } } },
            headers: { 'x-bar': { schema: { type: 'string' } } },
          },
        },
      },
    });
    expect(validator.schemas).toStrictEqual({
      '#/components/responses/bar/content/application~1json/schema': { type: 'number' },
      '#/components/responses/bar/headers/x-bar/schema': { type: 'string' },
      '#/components/responses/foo/content/application~1json/schema': { type: 'string' },
      '#/components/responses/foo/headers/x-foo/schema': { type: 'number' },
    });
  });

  it('should validate JSON schema refs', () => {
    const validator = createValidator({
      openapi: '3.0.2',
      info: { title: '', version: '' },
      paths: {},
      components: {
        schemas: {
          foo: {
            type: 'object',
            properties: { bar: { type: 'string' } },
          },
        },
      },
    });
    const result = validator.validate(
      { bar: '' },
      { $ref: '#/components/schemas/foo' },
      { base: '#' },
    );
    expect(result.valid).toBe(true);
  });

  it('should validate JSON schema refs containing special characters', () => {
    const validator = createValidator({
      openapi: '3.0.2',
      info: { title: '', version: '' },
      paths: {},
      components: {
        schemas: {
          'foo/bar~baz': {
            type: 'object',
            properties: { bar: { type: 'string' } },
          },
        },
      },
    });
    const result = validator.validate(
      { bar: '' },
      { $ref: '#/components/schemas/foo~1bar~0baz' },
      { base: '#' },
    );
    expect(result.valid).toBe(true);
  });
});
