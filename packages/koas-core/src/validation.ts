import { Validator, type ValidatorResult } from 'jsonschema';
import { type OpenAPIV3 } from 'openapi-types';

import { escapeJsonPointer } from './jsonRefs.js';

/**
 * Iterate over object entries.
 *
 * @param object The object to iterate over. This may be null.
 * @param iterator A function which will be called with the key and value.
 */
function iter<T>(object: T | null, iterator: (key: keyof T, value: T[keyof T]) => void): void {
  if (object) {
    for (const [key, value] of Object.entries(object)) {
      iterator(key as keyof T, value);
    }
  }
}

interface SchemaValidationErrorOptions {
  /**
   * The HTTP status code for the validation error.
   */
  status?: number;

  /**
   * The JSON schema validator result.
   */
  result: ValidatorResult;
}

/**
 * An error that’s thrown if JSON schema validation fails.
 */
export class SchemaValidationError extends Error {
  status: number;
  result: ValidatorResult;

  /**
   * @param message The error message to throw.
   * @param options The error options.
   */
  constructor(message: string, { result, status = 400 }: SchemaValidationErrorOptions) {
    super(message);
    this.name = 'SchemaValidationError';
    this.status = status;
    this.result = result;
  }
}

interface SchemaWrapper {
  /**
   * The JSON schema contained by the wrapper.
   */
  schema?: OpenAPIV3.ReferenceObject | OpenAPIV3.SchemaObject;
}

/**
 * @param document THe OpenAPI document to create a JSON schema validator for.
 * @returns A configured JSON schema validator.
 */
export function createValidator({ components = {} }: OpenAPIV3.Document): Validator {
  const validator = new Validator();

  // Register OpenAPI formats
  validator.customFormats.int32 = () => true;
  validator.customFormats.int64 = () => true;
  validator.customFormats.float = () => true;
  validator.customFormats.double = () => true;
  validator.customFormats.byte = () => true;
  validator.customFormats.binary = () => true;
  validator.customFormats.password = () => true;

  /**
   * Add schemas from a record of JSON schema property wrappers.
   *
   * @param wrappers The rescord that holds the JSON schema wrappers
   * @param prefix The prefix of the wrapper.
   */
  function procesSchemaWrapper(
    wrappers: Record<string, OpenAPIV3.ReferenceObject | SchemaWrapper>,
    prefix: string,
  ): void {
    iter(wrappers, (key, wrapper) => {
      if ('schema' in wrapper) {
        validator.addSchema(wrapper.schema, `${prefix}/${escapeJsonPointer(key)}/schema`);
      }
    });
  }

  iter(components.schemas, (key: string, schema) => {
    validator.addSchema(schema, `#/components/schemas/${escapeJsonPointer(key)}`);
  });
  procesSchemaWrapper(components.headers, '#/components/headers');
  procesSchemaWrapper(components.parameters, '#/components/parameters');
  iter(components.requestBodies, (key: string, requestBody) => {
    if ('content' in requestBody) {
      procesSchemaWrapper(
        requestBody.content,
        `#/components/requestBodies/${escapeJsonPointer(key)}/content`,
      );
    }
  });
  iter(components.responses, (key: string, response) => {
    if ('headers' in response) {
      procesSchemaWrapper(
        response.headers,
        `#/components/responses/${escapeJsonPointer(key)}/headers`,
      );
    }
    if ('content' in response) {
      procesSchemaWrapper(
        response.content,
        `#/components/responses/${escapeJsonPointer(key)}/content`,
      );
    }
  });

  return validator;
}
