# Koas Operations

Koas operations maps operation IDs to Koa controller functions.

## Installation

```sh
npm install koa koas-core koas-operations
```

## Usage

```js
const Koa = require('koa');
const { koas } = require('koas-core');
const { operations } = require('koas-operations');

const api = require('./api.json');
const controllers = require('./controllers');

const app = new Koa();
app.use(
  koas(api, [
    operations({
      controllers,
      async fallback(ctx, next) {
        // Handle operation not found
      },
    }),
  ]),
);
```

## Options

- `operations`: A mapping of operation IDs to Koa middleware functions.
- `fallback`: A fallback function. This is called if the operation object doesn’t have an
  `operationId` property or the operation ID doesn’t map to a function in the `operations` object.
- `throwOnExtraneous`: Throw an error if a handler is passed that doesn’t match an operation.
- `throwOnNotImplemented`: Throw an error if an operation hasn’t been implemented. (default `true`)
  (default `true`)
