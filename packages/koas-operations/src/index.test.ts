import { request, setTestApp } from 'axios-test-instance';
import * as Koa from 'koa';
import { koas } from 'koas-core';

import { operations } from './index.js';

const document = {
  openapi: '3.0.2',
  info: {
    title: 'Test server',
    version: 'test',
  },
  paths: {
    '/': {
      get: { operationId: 'test', responses: { 200: { description: '' } } },
      post: { responses: { 200: { description: '' } } },
      put: { operationId: 'invalid', responses: { 200: { description: '' } } },
    },
  },
};

describe('operations', () => {
  it('should not crash when no options are passed', () => {
    expect(operations).not.toThrow();
  });

  it('should throw if there are missing controllers', () => {
    expect(() => koas(document, [operations({ controllers: {} })])).toThrow(
      new Error('Missing controller for operationId: test'),
    );
  });

  it('should throw if an operation is found without an operationId', () => {
    expect(() =>
      koas(document, [operations({ controllers: { test: () => null, put: () => null } })]),
    ).toThrow(new Error('Detected an operation object without an operationId'));
  });

  it('should throw if an unused controller is found', () => {
    expect(() =>
      koas({ openapi: '', info: { title: '', version: '' }, paths: {} }, [
        operations({ controllers: { test: () => null, put: () => null } }),
      ]),
    ).toThrow(new Error('Found extraneous controller: test'));
  });

  describe('allow missing', () => {
    let controllers: Record<string, jest.Mock>;

    beforeEach(async () => {
      const app = new Koa();
      app.silent = true;
      controllers = {
        // @ts-expect-error THis type is invalid for testing purposes
        invalid: {},
        test: jest.fn(),
      };
      app.use(
        koas(document, [
          operations({ controllers, throwOnExtraneous: false, throwOnNotImplemented: false }),
        ]),
      );
      await setTestApp(app);
    });

    it('should call the matching operation', async () => {
      await request.get('/');
      expect(controllers.test).toHaveBeenCalledTimes(1);
    });

    it('should handle undefined operation ids as not implemented', async () => {
      const response = await request.post('/');
      expect(response.status).toBe(501);
    });

    it('should handle non-function implementations as not implemented', async () => {
      const response = await request.put('/');
      expect(response.status).toBe(501);
    });
  });

  describe('custom notImplemented', () => {
    let fallback: jest.Mock;

    beforeEach(async () => {
      fallback = jest.fn();
      const app = new Koa();
      app.use(
        koas(document, [
          operations({ fallback, throwOnExtraneous: false, throwOnNotImplemented: false }),
        ]),
      );
      await setTestApp(app);
    });

    it('should call the fallback if an undefined operation is not implemented', async () => {
      await request.post('/');
      expect(fallback).toHaveBeenCalledTimes(1);
    });

    it('should call the fallback if an operation has a non-function implementation', async () => {
      await request.put('/');
      expect(fallback).toHaveBeenCalledTimes(1);
    });
  });
});
