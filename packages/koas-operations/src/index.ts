import { type Context, type Middleware } from 'koa';
import { type Plugin } from 'koas-core';

export interface KoasOperationsOptions {
  /**
   * A mapping of OpenAPI operation IDs to controller functions.
   *
   * Controllers are normal Koa middleware, but typically these don’t have to call `next()`.
   */
  controllers?: Record<string, Middleware>;

  /**
   * The middleware to call in case the operation has no operation ID or no controller has been
   * registered for the operation.
   *
   * By default this is middleware that returns a status code of `501 Not Implemented`.
   */
  fallback?: Middleware;

  /**
   * Throw an error if a handler is passed that doesn’t match an operation.
   *
   * @default true
   */
  throwOnExtraneous?: boolean;

  /**
   * Throw an error if an operation hasn’t been implemented.
   *
   * @default true
   */
  throwOnNotImplemented?: boolean;
}

/**
 * Middleware which throws a status code `501 Not Implemented`.
 *
 * @param ctx The Koa context.
 */
function notImplemented(ctx: Context): void {
  ctx.throw(501);
}

/**
 * Map OpenAPI operation IDs to controllers.
 *
 * @param options The for controlling how operation IDs are handled.
 * @returns Koas middleware.
 */
export function operations({
  controllers = {},
  fallback = notImplemented,
  throwOnExtraneous = true,
  throwOnNotImplemented = true,
}: KoasOperationsOptions = {}): Plugin {
  return ({ document }) => {
    if (throwOnNotImplemented || throwOnExtraneous) {
      const operationObjects = Object.values(document.paths)
        .flatMap((path) => [
          path.delete,
          path.get,
          path.head,
          path.options,
          path.patch,
          path.post,
          path.put,
          path.trace,
        ])
        .filter(Boolean);
      if (throwOnNotImplemented) {
        for (const { operationId } of operationObjects) {
          if (typeof operationId !== 'string') {
            throw new TypeError('Detected an operation object without an operationId');
          }
          if (typeof controllers[operationId] !== 'function') {
            throw new TypeError(`Missing controller for operationId: ${operationId}`);
          }
        }
      }
      if (throwOnExtraneous) {
        for (const id of Object.keys(controllers)) {
          if (!operationObjects.some(({ operationId }) => id === operationId)) {
            throw new Error(`Found extraneous controller: ${id}`);
          }
        }
      }
    }
    return (ctx, next) => {
      const { operationObject } = ctx.openApi;
      const { operationId } = operationObject;
      if (typeof operationId !== 'string') {
        return fallback(ctx, next);
      }
      const operation = controllers[operationId];
      if (typeof operation !== 'function') {
        return fallback(ctx, next);
      }
      return operation(ctx, next);
    };
  };
}
