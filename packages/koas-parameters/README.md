# Koas Operations

Koas parameters attempts to coerce path and query parameters to the type specified in their
respective JSON schema.

## Installation

```sh
npm install koa koas-core koas-parameters
```

## Usage

```js
const Koa = require('koa');
const { koas } = require('koas-core');
const { parameters } = require('koas-parameters');

const api = require('./api.json');

const app = new Koa();
app.use(
  koas(api, [
    parameters({
      parsers: {
        // Additional parsers
      },
    }),
  ]),
);
```

## Options

- `parsers`: A mapping of JSON schema types to parsers. This allows to override how certain values
  are parsed. In most cases, the defaults will suffice.
