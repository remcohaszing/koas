import { request, setTestApp } from 'axios-test-instance';
import * as Koa from 'koa';
import { koas } from 'koas-core';
import { type OpenAPIV3 } from 'openapi-types';

import { parameters } from './index.js';

const document: OpenAPIV3.Document = {
  openapi: '3.0.2',
  info: {
    title: 'Test server',
    version: 'test',
  },
  paths: {
    '/bool/{value}': {
      parameters: [{ name: 'value', in: 'path', required: true, schema: { type: 'boolean' } }],
      get: { responses: { 200: { description: '' } } },
    },
    '/int/{value}': {
      parameters: [{ name: 'value', in: 'path', required: true, schema: { type: 'integer' } }],
      get: { responses: { 200: { description: '' } } },
    },
    '/numeric/{value}': {
      parameters: [{ name: 'value', in: 'path', required: true, schema: { type: 'number' } }],
      get: { responses: { 200: { description: '' } } },
    },
    '/str/{value}': {
      parameters: [{ name: 'value', in: 'path', required: true, schema: { type: 'string' } }],
      get: { responses: { 200: { description: '' } } },
    },
    '/bool': {
      parameters: [{ name: 'value', in: 'query', schema: { type: 'boolean' } }],
      get: { responses: { 200: { description: '' } } },
    },
    '/int': {
      parameters: [{ name: 'value', in: 'query', schema: { type: 'integer' } }],
      get: { responses: { 200: { description: '' } } },
    },
    '/numeric': {
      parameters: [{ name: 'value', in: 'query', schema: { type: 'number' } }],
      get: { responses: { 200: { description: '' } } },
    },
    '/str': {
      parameters: [{ name: 'value', in: 'query', schema: { type: 'string' } }],
      get: { responses: { 200: { description: '' } } },
    },
    '/bools': {
      parameters: [
        { name: 'value', in: 'query', schema: { type: 'array', items: { type: 'boolean' } } },
      ],
      get: { responses: { 200: { description: '' } } },
    },
    '/ints': {
      parameters: [
        { name: 'value', in: 'query', schema: { type: 'array', items: { type: 'integer' } } },
      ],
      get: { responses: { 200: { description: '' } } },
    },
    '/numerics': {
      parameters: [
        { name: 'value', in: 'query', schema: { type: 'array', items: { type: 'number' } } },
      ],
      get: { responses: { 200: { description: '' } } },
    },
    '/strs': {
      parameters: [
        { name: 'value', in: 'query', schema: { type: 'array', items: { type: 'string' } } },
      ],
      get: { responses: { 200: { description: '' } } },
    },
  },
};

let pathParams: unknown;
let queryParams: unknown;

beforeEach(async () => {
  const app = new Koa();
  app.use(
    koas(document, [
      parameters(),
      () => (ctx, next) => {
        ({ pathParams, queryParams } = ctx);
        return next();
      },
    ]),
  );
  await setTestApp(app);
});

afterEach(() => {
  pathParams = undefined;
  queryParams = undefined;
});

describe('path', () => {
  it('should parse a boolean value true', async () => {
    await request.get('/bool/true');
    expect(pathParams).toStrictEqual({ value: true });
  });

  it('should parse a boolean value false', async () => {
    await request.get('/bool/false');
    expect(pathParams).toStrictEqual({ value: false });
  });

  it('should parse an integer', async () => {
    await request.get('/int/42');
    expect(pathParams).toStrictEqual({ value: 42 });
  });

  it('should parse a number', async () => {
    await request.get('/numeric/13.37');
    expect(pathParams).toStrictEqual({ value: 13.37 });
  });

  it('should leave a string as-is', async () => {
    await request.get('/str/1337');
    expect(pathParams).toStrictEqual({ value: '1337' });
  });

  it('should validate path parameters', async () => {
    const { data, status } = await request.get('/int/13.37');
    expect(status).toBe(400);
    expect(data).toStrictEqual({
      message: 'Path parameter validation failed',
      errors: [
        {
          argument: ['integer'],
          instance: '13.37',
          message: 'is not of a type(s) integer',
          name: 'type',
          path: ['value'],
          property: 'instance.value',
          schema: { type: 'integer' },
          stack: 'instance.value is not of a type(s) integer',
        },
      ],
    });
  });
});

describe('query', () => {
  it('should parse a boolean value true', async () => {
    await request.get('/bool', { params: { value: 'true' } });
    expect(queryParams).toStrictEqual({ value: true });
  });

  it('should parse a boolean value false', async () => {
    await request.get('/bool', { params: { value: 'false' } });
    expect(queryParams).toStrictEqual({ value: false });
  });

  it('should parse an integer', async () => {
    await request.get('/int', { params: { value: '42' } });
    expect(queryParams).toStrictEqual({ value: 42 });
  });

  it('should parse a number', async () => {
    await request.get('/numeric', { params: { value: '13.37' } });
    expect(queryParams).toStrictEqual({ value: 13.37 });
  });

  it('should leave a string as-is', async () => {
    await request.get('/str', { params: { value: '1337' } });
    expect(queryParams).toStrictEqual({ value: '1337' });
  });

  it('should parse a boolean array value', async () => {
    await request.get('/bools', { params: { value: 'true' } });
    expect(queryParams).toStrictEqual({ value: [true] });
  });

  it('should parse an integer array', async () => {
    await request.get('/ints', { params: { value: '42' } });
    expect(queryParams).toStrictEqual({ value: [42] });
  });

  it('should parse a number array', async () => {
    await request.get('/numerics', { params: { value: '13.37' } });
    expect(queryParams).toStrictEqual({ value: [13.37] });
  });

  it('should parse a string array', async () => {
    await request.get('/strs', { params: { value: '1337' } });
    expect(queryParams).toStrictEqual({ value: ['1337'] });
  });

  it('should validate query parameters', async () => {
    const { data, status } = await request.get('/int', { params: { value: 13.37 } });
    expect(status).toBe(400);
    expect(data).toStrictEqual({
      message: 'Query parameter validation failed',
      errors: [
        {
          argument: ['integer'],
          instance: '13.37',
          message: 'is not of a type(s) integer',
          name: 'type',
          path: ['value'],
          property: 'instance.value',
          schema: { type: 'integer' },
          stack: 'instance.value is not of a type(s) integer',
        },
      ],
    });
  });
});

describe('custom parsers', () => {
  beforeEach(async () => {
    const app = new Koa();
    app.use(
      koas(document, [
        parameters({ parsers: { string: (value) => `${value}-postfix` } }),
        () => (ctx, next) => {
          ({ pathParams } = ctx);
          return next();
        },
      ]),
    );
    await setTestApp(app);
  });

  it('should allow custom parsers', async () => {
    await request.get('/str/value');
    expect(pathParams).toStrictEqual({ value: 'value-postfix' });
  });
});
