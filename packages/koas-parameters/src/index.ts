import { type Context } from 'koa';
import { type Plugin } from 'koas-core';
import { type OpenAPIV3 } from 'openapi-types';

import { createParameterHandler } from './utils.js';

/**
 * This interface may be augmented to define path parameter types.
 */
// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface PathParams {}

/**
 * This interface may be augmented to define query parameter types.
 */
// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface QueryParams {}

declare module 'koa' {
  interface DefaultContext {
    /**
     * The path parameters coerced according to the type defined in their JSON schema in the OpenAPI
     * document.
     */
    pathParams: PathParams;

    /**
     * The query parameters coerced according to the type defined in their JSON schema in the
     * OpenAPI document.
     */
    queryParams: QueryParams;
  }
}

export interface ParameterParsers {
  /**
   * A function to convert a raw string parameter to a boolean.
   *
   * @param value The raw string value to process.
   * @returns The processed boolean value.
   */
  boolean?: (value: string) => boolean | string;

  /**
   * A function to convert a raw string parameter to an integer.
   *
   * @param value The raw string value to process.
   * @returns The processed integer value.
   */
  integer?: (value: string) => number | string;

  /**
   * A function to convert a raw string parameter to number.
   *
   * @param value The raw string value to process.
   * @returns The processed number value.
   */
  number?: (value: string) => number | string;

  /**
   * A function to convert a raw string parameter to a processed string.
   *
   * @param value The raw string value to process.
   * @returns The processed string value.
   */
  string?: (value: string) => string;
}

export interface KoasParametersOptions {
  /**
   * The parsers for converting raw values to strings.
   */
  parsers?: ParameterParsers;
}

const defaultParsers: ParameterParsers = {
  boolean(value) {
    switch (value) {
      case 'false':
        return false;
      case 'true':
        return true;
      default:
        return value;
    }
  },
  integer(value) {
    const integer = Number(value);
    if (Number.isInteger(integer)) {
      return integer;
    }
    return value;
  },
  number(value) {
    const number = Number(value);
    if (Number.isFinite(number)) {
      return number;
    }
    return value;
  },
  string: String,
};

/**
 * Create a Koas plugin for parsing path and query parameters.
 *
 * @param options The options for parsing the parameters.
 * @returns A Koas plugin for parsing path and query parameters.
 */
export function parameters({ parsers = {} }: KoasParametersOptions = {}): Plugin {
  const combinedParsers = {
    ...defaultParsers,
    ...parsers,
  };

  return ({ document, resolveRef, validate }) => {
    const handlers = new Map<OpenAPIV3.OperationObject, (ctx: Context) => void>();
    for (const {
      delete: del,
      get,
      head,
      options,
      parameters: parametersObjects,
      patch,
      post,
      put,
      trace,
    } of Object.values(document.paths)) {
      for (const operationObject of [del, get, head, options, patch, post, put, trace]) {
        if (operationObject) {
          handlers.set(
            operationObject,
            createParameterHandler(
              operationObject,
              parametersObjects,
              combinedParsers,
              validate,
              resolveRef,
            ),
          );
        }
      }
    }

    return (ctx, next) => {
      const { operationObject } = ctx.openApi;
      if (handlers.has(operationObject)) {
        const handler = handlers.get(operationObject);
        handler(ctx);
      }
      return next();
    };
  };
}
