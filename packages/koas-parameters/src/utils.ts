import { type Context } from 'koa';
import { type JSONRefResolver, type Validator } from 'koas-core';
import { type OpenAPIV3 } from 'openapi-types';

import { type ParameterParsers } from './index.js';

/**
 * Create a full JSON schema for objects from a list of OpenAPI parameter objects.
 *
 * @param definitions The parameter objects to create a schema from.
 * @returns The resulting  JSON schema.
 */
function createSchema(definitions: OpenAPIV3.ParameterObject[]): OpenAPIV3.SchemaObject {
  if (definitions.length === 0) {
    return null;
  }
  const r = definitions.filter(({ required }) => required).map(({ name }) => name);
  const fullSchema: OpenAPIV3.SchemaObject = {
    type: 'object',
    additionalProperties: true,
    properties: Object.fromEntries(definitions.map(({ name, schema = {} }) => [name, schema])),
  };
  if (r.length) {
    fullSchema.required = r;
  }
  return fullSchema;
}

/**
 * Create a function for parsing and validating query and path parameters.
 *
 * @param operationObject The active OpenAPI operation object.
 * @param parameters The active OpenAPI parameters object.
 * @param parsers Provided parameter parser functions.
 * @param validate The schema validator function.
 * @param resolveRef A JSON reference resolver.
 * @returns A function for parsing and validating query and path parameters.
 */
export function createParameterHandler(
  operationObject: OpenAPIV3.OperationObject,
  parameters: (OpenAPIV3.ParameterObject | OpenAPIV3.ReferenceObject)[],
  parsers: ParameterParsers,
  validate: Validator,
  resolveRef: JSONRefResolver,
): (ctx: Context) => void {
  const queryDefinitions: OpenAPIV3.ParameterObject[] = [];
  const pathDefinitions: OpenAPIV3.ParameterObject[] = [];
  for (const maybeRef of [...(parameters || []), ...(operationObject.parameters || [])]) {
    const definition = resolveRef(maybeRef);
    if (definition.in === 'query') {
      queryDefinitions.push(definition);
    } else if (definition.in === 'path') {
      pathDefinitions.push(definition);
    }
  }
  const querySchema = createSchema(queryDefinitions);
  const pathSchema = createSchema(pathDefinitions);

  /**
   * Parse a valud using a parser function based on the type.
   *
   * @param value The value to parse.
   * @param type The type of the SJON schma.
   * @returns The parsed value.
   */
  function parse(value: string, type = 'string'): unknown {
    if (Object.hasOwnProperty.call(parsers, type)) {
      return parsers[type as keyof ParameterParsers](value);
    }
    return value;
  }

  /**
   * Parse a parameters object based on a JSON schema.
   *
   * @param rawParams The raw parameters to parse.
   * @param schema The JSON schema to base tpe conversions on.
   * @param message The schema validation message to throw.
   * @returns The parsed object.
   */
  function parseObject(
    rawParams: Record<string, string[] | string>,
    schema: OpenAPIV3.SchemaObject,
    message: string,
  ): Record<string, unknown> {
    if (!schema) {
      return;
    }
    const params: Record<string, unknown> = {};
    for (const [key, value] of Object.entries(rawParams)) {
      if (!Object.hasOwnProperty.call(schema.properties, key)) {
        params[key] = value;
        continue;
      }
      const subSchema = resolveRef(schema.properties[key]);
      if (subSchema.type === 'array') {
        const values = Array.isArray(value) ? value : [value];
        params[key] = values.map((val) => parse(val, resolveRef(subSchema.items).type));
        continue;
      }
      if (Array.isArray(value)) {
        params[key] = value;
        continue;
      }
      params[key] = parse(value, subSchema.type);
    }
    validate(params, schema, { message });
    return params;
  }

  return (ctx) => {
    ctx.pathParams = parseObject(ctx.params, pathSchema, 'Path parameter validation failed');
    ctx.queryParams = parseObject(ctx.query, querySchema, 'Query parameter validation failed');
  };
}
