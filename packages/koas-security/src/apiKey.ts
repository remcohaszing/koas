import { type Context } from 'koa';
import { type OpenAPIV3 } from 'openapi-types';
import { type Promisable } from 'type-fest';

import { type SecurityCheck } from './types.js';

export type GetApiKeyUser<User = unknown, Client = unknown> = (
  apiKey: string,
  ctx: Context,
) => Promisable<User | [User, Client?]>;

/**
 * Get a user based on an apiKey security scheme.
 *
 * @param scheme The OpenAPI security scheme.
 * @param userGetter A function for getting a user.
 * @returns A tuple containing just the user.
 */
export function apiKeySecurityCheck(
  scheme: OpenAPIV3.ApiKeySecurityScheme,
  userGetter: GetApiKeyUser,
): SecurityCheck {
  return async (ctx) => {
    let apiKey: string[] | string;

    switch (scheme.in) {
      case 'cookie':
        apiKey = ctx.cookies.get(scheme.name);
        break;
      case 'header':
        apiKey = ctx.headers[scheme.name.toLowerCase()];
        break;
      case 'query':
        apiKey = ctx.query[scheme.name];
        break;
      default:
        return;
    }

    if (!apiKey) {
      return;
    }

    if (typeof apiKey !== 'string') {
      return;
    }

    const user = await userGetter(apiKey, ctx);
    return Array.isArray(user) ? (user as [unknown, unknown]) : [user];
  };
}
