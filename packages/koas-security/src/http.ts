import { type Context } from 'koa';
import { type OpenAPIV3 } from 'openapi-types';
import { type Promisable } from 'type-fest';

import { type SecurityCheck } from './types.js';
import { parseAuthorizationHeader } from './utils.js';

type GetHttpBasicUser<User = unknown, Client = unknown> = (
  username: string,
  password: string,
  ctx: Context,
) => Promisable<User | [User, Client?]>;

type GetHttpBearerUser<User = unknown, Client = unknown> = (
  accessToken: string,
  ctx: Context,
) => Promisable<User | [User, Client?]>;

export type GetHttpUser<User = unknown, Client = unknown> =
  | GetHttpBasicUser<User, Client>
  | GetHttpBearerUser<User, Client>;

/**
 * Get a user based on an http security scheme.
 *
 * @param scheme The OpenAPI security scheme.
 * @param userGetter A function for getting a user.
 * @returns A tuple containing just the user.
 */
export function httpSecurityCheck(
  scheme: OpenAPIV3.HttpSecurityScheme,
  userGetter: GetHttpUser,
): SecurityCheck {
  return async (ctx) => {
    const [type, credentials] = parseAuthorizationHeader(ctx);
    let user: unknown;
    if (type.toLowerCase() !== scheme.scheme) {
      return null;
    }

    switch (type) {
      case 'Basic': {
        const basicAuthMatch = String(Buffer.from(credentials, 'base64')).match(/([^:]*):(.*)/);
        if (!basicAuthMatch) {
          return null;
        }
        user = await (userGetter as GetHttpBasicUser)(basicAuthMatch[1], basicAuthMatch[2], ctx);
        break;
      }
      case 'Bearer':
        user = await (userGetter as GetHttpBearerUser)(credentials, ctx);
        break;
      default:
        // Unsupported authentication scheme.
        return null;
    }

    return Array.isArray(user) ? (user as [unknown, unknown]) : [user];
  };
}
