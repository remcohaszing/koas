import { type Plugin } from 'koas-core';

import { apiKeySecurityCheck, GetApiKeyUser } from './apiKey.js';
import { GetHttpUser, httpSecurityCheck } from './http.js';
import { GetOAuth2User, oauth2SecurityCheck } from './oauth2.js';
import { OAuth2Client, type SecurityCheck } from './types.js';

/**
 * Known clients that may be assigned to `ctx.client` and `ctx.clients`.
 */
// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface Clients {}

/**
 * Known clients that may be assigned to `ctx.user` and `ctx.users`.
 */
// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface Users {}

/**
 * A function for getting a user.
 */
export type GetUser<User = unknown, Client = unknown> =
  | GetApiKeyUser<User, Client>
  | GetHttpUser<User, Client>
  | GetOAuth2User<User, Client>;

export { GetApiKeyUser, GetHttpUser, GetOAuth2User, OAuth2Client };

export type SecurityOptions = {
  [K in keyof Users]: GetUser<Users[K], Clients[K]>;
};

/**
 * Authenticate users based on OpenAPI security schemes and security requirements.
 *
 * @param options A mapping of OpenAPI security definition keys to user getter functions.
 * @returns A Koas plugn for authenticating users.
 */
export function security(options: SecurityOptions): Plugin {
  return ({ document, resolveRef }) => {
    const securityChecks: Record<string, SecurityCheck> = {};

    // Work around the fact that the unaugmented options have no keys.
    const opts = options as Record<string, GetUser>;
    for (const [key, schemeOrRef] of Object.entries(document.components.securitySchemes || [])) {
      if (!opts[key]) {
        throw new Error(`No user getter was defined for security scheme: ${key}`);
      }
      const scheme = resolveRef(schemeOrRef);
      switch (scheme.type) {
        case 'apiKey':
          securityChecks[key] = apiKeySecurityCheck(scheme, opts[key] as GetApiKeyUser);
          break;
        case 'http':
          securityChecks[key] = httpSecurityCheck(scheme, opts[key] as GetHttpUser);
          break;
        case 'oauth2':
        case 'openIdConnect':
          securityChecks[key] = oauth2SecurityCheck(scheme, opts[key] as GetOAuth2User);
          break;
        default:
          throw new Error(`Unknown security scheme: ${JSON.stringify(scheme)}`);
      }
    }

    return async (ctx, next) => {
      const { operationObject } = ctx.openApi;
      if (!operationObject?.security) {
        return next();
      }
      for (const securityRequirementObject of operationObject.security) {
        const users: Record<string, unknown> = {};
        const clients: Record<string, unknown> = {};
        try {
          await Promise.all(
            Object.entries(securityRequirementObject).map(async ([key, requiredScopes]) => {
              const pair = await securityChecks[key](ctx, requiredScopes);
              if (!pair?.[0]) {
                throw new Error(`Unauthorized using security requirement: ${key}`);
              }
              [users[key]] = pair;
              if (pair[1]) {
                [, clients[key]] = pair;
              }
            }),
          );
        } catch {
          continue;
        }
        ctx.users = users;
        ctx.clients = clients;
        // @ts-expect-error The type of ctx.user is never, because it’s not augmented yet.
        [ctx.user] = Object.values(users);
        // @ts-expect-error The type of ctx.client is never, because it’s not augmented yet.
        [ctx.client] = Object.values(clients);
        ctx.openApi.securityRequirementObject = securityRequirementObject;
        return next();
      }
      return ctx.throw(401);
    };
  };
}
