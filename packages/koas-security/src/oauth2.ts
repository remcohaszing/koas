import { type Context } from 'koa';
import { type OpenAPIV3 } from 'openapi-types';
import { type Promisable } from 'type-fest';

import { type OAuth2Client, type SecurityCheck } from './types.js';
import { parseAuthorizationHeader } from './utils.js';

export type GetOAuth2User<User = unknown, Client = unknown> = (
  accessToken: string,
  ctx: Context,
) => Promisable<[User, Client & OAuth2Client]>;

/**
 * Get a user based on an oauth2 security scheme.
 *
 * @param scheme The OpenAPI security scheme.
 * @param userGetter A function for getting a user.
 * @returns A tuple containing the user and the OAuth2 client.
 */
export function oauth2SecurityCheck(
  scheme: OpenAPIV3.OAuth2SecurityScheme | OpenAPIV3.OpenIdSecurityScheme,
  userGetter: GetOAuth2User,
): SecurityCheck {
  return async (ctx, scopes) => {
    const [type, accessToken] = parseAuthorizationHeader(ctx);
    if (type !== 'Bearer') {
      return null;
    }

    const pair = await userGetter(accessToken, ctx);
    if (!pair || pair.length < 2) {
      return null;
    }
    const [user, client] = pair;
    const clientScopes = new Set(
      typeof client.scope === 'string' ? client.scope.split(/\s+/) : client.scope,
    );
    const isValid = scopes.every((scope) => clientScopes.has(scope));
    return isValid ? [user, client] : null;
  };
}
