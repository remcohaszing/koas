import { type Context } from 'koa';
import { type OpenAPIV3 } from 'openapi-types';
import { type Promisable } from 'type-fest';

import { type Clients, type Users } from './index.js';

/**
 * A single client
 */
// export type Client = Clients[keyof Clients];

/**
 * A single user
 */
// export type User = Users[keyof Users];

/**
 * A representation of an OAuth2 client.
 */
export interface OAuth2Client {
  /**
   * The scope this client is allowed to use.
   *
   * This may be a list or a space separated string.
   */
  scope: Set<string> | string[] | string;
}

export type SecurityCheck = (ctx: Context, scopes?: string[]) => Promisable<[unknown, unknown?]>;

declare module 'koa' {
  interface DefaultContext {
    /**
     * This is one of the authenticated clients.
     *
     * Use `clients` if multiple security requirements need to be met.
     *
     * This property is injected by `koas-security`.
     */
    client?: Clients[keyof Clients];

    /**
     * A mapping of OpenAPI security schemes to authenticated clients.
     *
     * Usually only one security requirement needs to be met. In this case using `client` is
     * preferred.
     *
     * This property is injected by `koas-security`.
     */
    clients?: Clients;

    /**
     * This is one of the authenticated users.
     *
     * Use `users` if multiple security requirements need to be met.
     *
     * This property is injected by `koas-security`.
     */
    user?: Users[keyof Users];

    /**
     * A mapping of OpenAPI security schemes to authenticated users.
     *
     * Usually only one security requirement needs to be met. In this case using `user` is
     * preferred.
     *
     * This property is injected by `koas-security`.
     */
    users?: Users;
  }
}

declare module 'koas-core' {
  interface OpenAPIContext {
    /**
     * The security requirements object that is active in the current context.
     *
     * This property is injected by `koas-security`.
     */
    securityRequirementObject?: OpenAPIV3.SecurityRequirementObject;
  }
}
