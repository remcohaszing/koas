import { type Context } from 'koa';

const authorizationRegExp = /^(\S+) (.*)$/;

/**
 * Patse the authorization token from the Koa context.
 *
 * @param ctx The Koa context containing the token.
 * @returns A tuple containing the type and value of the authorization header.
 */
export function parseAuthorizationHeader(ctx: Context): [string, string] {
  const match = authorizationRegExp.exec(ctx.header.authorization);
  if (!match) {
    return ['', null];
  }
  return match.slice(1) as [string, string];
}
