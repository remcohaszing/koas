# Koas Operations

Koas serializer converts a response body to the negotiated response format.

## Installation

```sh
npm install koa koas-core koas-serializer
```

## Usage

```js
const Koa = require('koa');
const { koas } = require('koas-core');
const { serializer } = require('koas-serializer');

const api = require('./api.json');

const app = new Koa();
app.use(
  koas(api, [
    serializer({
      // Serializers
    }),
  ]),
);
```

## Options

Koas serializer accepts a mapping of mime types to serializer functions. Serializer functions should
accept the request body, and return a string or buffer.

The following serializers are supported by default:

- `application/json`: The body is converted to JSON using the builtin `JSON.stringify`.
- `text/*`: If the body is a buffer, it is returned as-is. Otherwise it is converted to a string
  using the `toString()` method.
