import { type Context } from 'koa';
import { type Plugin } from 'koas-core';
import { is } from 'type-is';

/**
 * A function to serialize a response body.
 *
 * @param body The body that was set on the Koa context.
 * @param ctx The Koa context.
 * @returns The serialized body.
 */
export type Serializer = (body: unknown, ctx: Context) => Buffer | string;

export interface SerializeOptions {
  /**
   * A mapping content types to serializer functions.
   *
   * The key represents a content type. An asterisk is allowed to use it for multiple similar
   * content types.
   */
  serializers?: Record<string, Serializer>;
}

const defaultSerializers: Record<string, Serializer> = {
  'application/json': (body) => JSON.stringify(body),
  'text/*': (body) => (body instanceof Buffer ? body : String(body)),
};

/**
 * Serialize a response body based on the accept header.
 *
 * Default serializers have been registered for `application/json` and `text/*`.
 *
 * @param options The options for serializeing the response body.
 * @returns A Koas plugin which serializes the response body bases on the accept header.
 */
export function serializer({ serializers = {} }: SerializeOptions = {}): Plugin {
  const allSerializers = {
    ...defaultSerializers,
    ...serializers,
  };

  const supportedMimeTypes = Object.keys(allSerializers).sort((a, b) => (a > b ? -1 : 1));

  return ({ resolveRef }) =>
    async (ctx: Context, next) => {
      await next();
      const { body, openApi, request } = ctx;
      const { operationObject } = openApi;
      const responseObject = resolveRef(
        operationObject.responses[ctx.status] || operationObject.responses.default,
      );
      if (responseObject?.content) {
        const contentType = request.accepts(Object.keys(responseObject.content));
        ctx.assert(contentType, 406);
        const type = supportedMimeTypes.find((m) => is(contentType as string, m));
        // If type is null, this means a type was specified in the api specification, but no
        // serializer has been registered. Let’s assume in this case a developer has serialized the
        // body some other way.
        if (type != null) {
          ctx.body = allSerializers[type](body, ctx);
          ctx.type = contentType as string;
        }
      }
    };
}
