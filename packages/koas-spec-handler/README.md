# Koas Spec Handler

Koas spec handler exposes the Open API document as a JSON API call.

## Installation

```sh
npm install koa koas-core koas-spec-handler
```

## Usage

```js
const Koa = require('koa');
const { koas } = require('koas-core');
const { specHandler } = require('koas-spec-handler');

const api = require('./api.json');

const app = new Koa();
app.use(koas(api, [specHandler()]));
```

## Options

Koas spec handler accepts one single argument. This is a string that specifies on which URL the Open
API document is exposed. By default, it is exposed on `/api.json`.
