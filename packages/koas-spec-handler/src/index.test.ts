import { request, setTestApp } from 'axios-test-instance';
import * as Koa from 'koa';
import { koas } from 'koas-core';

import { specHandler } from './index.js';

const document = {
  openapi: '3.0.2',
  info: {
    title: 'koas-spec-handler',
    version: 'test',
  },
  paths: {},
};

it('should serve the OpenAPI document on /api.json by default', async () => {
  const app = new Koa();
  app.use(koas(document, [specHandler()]));
  await setTestApp(app);
  const response = await request.get('/api.json');
  expect(response.data).toStrictEqual(document);
});

it('should be possible to define a custom url', async () => {
  const app = new Koa();
  app.use(koas(document, [specHandler({ url: '/custom-url' })]));
  await setTestApp(app);
  const response = await request.get('/custom-url');
  expect(response.data).toStrictEqual(document);
});

it('should call the next middleware if there is no match', async () => {
  const app = new Koa();
  app.use(koas(document, [specHandler()]));
  await setTestApp(app);
  const response = await request.get('/');
  expect(response.status).toBe(404);
});
