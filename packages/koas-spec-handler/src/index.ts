import { type Plugin } from 'koas-core';

declare module 'koas-core' {
  interface OpenAPIContext {
    /**
     * The URL on which the OpenAPI document is served.
     */
    specURL?: string;
  }
}

interface SpecHandlerOptions {
  /**
   * The URL on which to serve the OpenAPI document.
   */
  url?: string;
}

/**
 * Serve the Koas OpenAPI document as JSON.
 *
 * @param options Options to configure how the OpenAPI document is served.
 * @returns A Koas plugin to serve the OpenAPI document.
 */
export function specHandler({ url = '/api.json' }: SpecHandlerOptions = {}): Plugin {
  return ({ document, runAlways }) =>
    runAlways((ctx, next) => {
      ctx.openApi.specURL = url;
      if (ctx.url !== url || ctx.method !== 'GET') {
        return next();
      }
      ctx.body = document;
    });
}
