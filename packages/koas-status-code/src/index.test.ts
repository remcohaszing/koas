import { request, setTestApp } from 'axios-test-instance';
import * as Koa from 'koa';
import { koas } from 'koas-core';

import { statusCode } from './index.js';

const document = {
  openapi: '3.0.2',
  info: {
    title: 'test',
    version: 'test',
  },
  paths: {
    '/create': {
      post: {
        responses: {
          201: { description: '' },
          404: { description: '' },
        },
      },
    },
    '/explicit': {
      get: {
        responses: {
          201: { description: '' },
        },
      },
    },
    '/invalid-code': {
      get: {
        responses: {
          200.1: { description: '' },
        },
      },
    },
    '/below-200': {
      get: {
        responses: {
          199: { description: '' },
        },
      },
    },
    '/override': {
      get: {
        responses: {
          201: { description: '' },
        },
      },
    },
  },
};

describe('statusCode', () => {
  beforeEach(async () => {
    const app = new Koa();
    app.use(
      koas(document, [
        () => (ctx, next) => {
          if (ctx.url === '/explicit') {
            ctx.status = 204;
          }
          return next();
        },
        statusCode(),
        () => (ctx, next) => {
          if (ctx.url === '/override') {
            ctx.status = 204;
          }
          return next();
        },
      ]),
    );
    await setTestApp(app);
  });

  it('should set a default success status code', async () => {
    const response = await request.post('/create');
    expect(response.status).toBe(201);
  });

  it('should not override explicit status codes', async () => {
    const response = await request.get('/explicit');
    expect(response.status).toBe(204);
  });

  it('should not allow later midlleware to override the status code', async () => {
    const response = await request.get('/override');
    expect(response.status).toBe(204);
  });

  it('should ignore status codes below 200', async () => {
    const response = await request.get('/below-200');
    expect(response.status).toBe(404);
  });

  it('should ignore non-integer status codes', async () => {
    const response = await request.get('/invalid-code');
    expect(response.status).toBe(404);
  });

  it('should throw an error if multiple successful status codes are defined', () => {
    expect(() =>
      koas(
        {
          openapi: '3.0.3',
          info: null,
          paths: {
            '/foo': {
              get: {
                responses: {
                  200: null,
                  201: null,
                },
              },
            },
          },
        },
        [statusCode()],
      ),
    ).toThrow(new Error('Operation "#/paths//foo/get" has defined multiple success status codes'));
  });
});
