import { type Plugin } from 'koas-core';
import { type OpenAPIV3 } from 'openapi-types';

const methods = ['delete', 'get', 'head', 'options', 'patch', 'post', 'put', 'trace'] as const;

/**
 * Create a map that maps an OpenAPI operation object to a succesful status code.
 *
 * @param document The OpenAPI document to process.
 * @returns A map that maps OpenAPI operation objects to a status code.
 */
function makeStatusMap(document: OpenAPIV3.Document): Map<OpenAPIV3.OperationObject, number> {
  const statusMap = new Map();
  for (const [pathTemplate, pathItem] of Object.entries(document.paths)) {
    for (const method of methods) {
      if (!Object.hasOwnProperty.call(pathItem, method)) {
        continue;
      }
      const operationObject = pathItem[method];
      for (const key of Object.keys(operationObject.responses)) {
        const status = Number(key);
        if (!Number.isInteger(status)) {
          continue;
        }
        if (status < 200) {
          continue;
        }
        if (status >= 300) {
          continue;
        }
        if (statusMap.has(operationObject)) {
          throw new Error(
            `Operation "#/paths/${pathTemplate}/${method}" has defined multiple success status codes`,
          );
        }
        statusMap.set(operationObject, status);
      }
    }
  }
  return statusMap;
}

/**
 * Get a Koas plugin that automatically sets the success status code for operations.
 *
 * @returns A Koas plugin that automatically sets the success status code.
 */
export function statusCode(): Plugin {
  return ({ document }) => {
    const statusCodeMap = makeStatusMap(document);

    return (ctx, next) => {
      const { operationObject } = ctx.openApi;
      if (
        statusCodeMap.has(operationObject) &&
        // @ts-expect-error This is an undocumented private property, but it works.
        // eslint-disable-next-line no-underscore-dangle
        !ctx.response._explicitStatus
      ) {
        ctx.status = statusCodeMap.get(operationObject);
      }
      return next();
    };
  };
}
