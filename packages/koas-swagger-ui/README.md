# Koas Swagger UI

Koas Swagger UI serves Swagger UI. This requires the `specURL` variable to have been set on the
`ctx.openApi` object. Typically this is done by combining it with [`koas-spec-handler`][].

> **Note**: Since this serves static files from [`swagger-ui-dist`][], this can’t be used with a
> module bundler.

## Installation

```sh
npm install koa koas-core koas-spec-handler koas-swagger-ui
```

## Usage

```js
const Koa = require('koa');
const { koas } = require('koas-core');
const { specHandler } = require('koas-spec-handler');
const { swaggerUI } = require('koas-swagger-uit');

const api = require('./api.json');

const app = new Koa();
app.use(
  koas(api, [
    specHandler(),
    swaggerUI({
      plugins: [
        // Plugins
      ],
      presets: [
        // Presets
      ],
      url: '',
    }),
  ]),
);
```

## Options

- `plugins`: This is a list of strings which specifies which plugins should be loaded into Swagger
  UI. The values can be taken from the `koasSwaggerUI.plugins` mapping.
- `presets`: This is a list of strings which specifies which presets should be loaded into Swagger
  UI. The values can be taken from the `koasSwaggerUI.presets` mapping.
- `url`: The URL on which Swagger UI is hosted. By default this is hosted on the root URL.

[`koas-spec-handler`]: https://www.npmjs.com/package/koas-spec-handler
[`swagger-ui-dist`]: https://www.npmjs.com/package/swagger-ui-dist
