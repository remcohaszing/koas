import { readFileSync } from 'node:fs';
import { join } from 'node:path';

import { request, setTestApp } from 'axios-test-instance';
import * as Koa from 'koa';
import { koas } from 'koas-core';
import { specHandler } from 'koas-spec-handler';
import { absolutePath } from 'swagger-ui-dist';

import { swaggerUI } from './index.js';

const assets = Object.fromEntries(
  [
    'favicon-16x16.png',
    'favicon-32x32.png',
    'swagger-ui-bundle.js',
    'swagger-ui-bundle.js.map',
    'swagger-ui.css',
    'swagger-ui.css.map',
    'swagger-ui-standalone-preset.js',
    'swagger-ui-standalone-preset.js.map',
  ].map((filename) => [filename, readFileSync(join(absolutePath(), filename))]),
);

const document = {
  openapi: '3.0.2',
  info: {
    title: 'Test server',
    version: 'test',
  },
  paths: {},
};

describe.each([
  ['/', '/'],
  ['/foo', '/'],
  ['/foo/', '/foo/'],
  ['/foo/bar', '/foo/'],
  ['/foo/bar/', '/foo/bar/'],
])('given url %s', (url, prefix) => {
  it.each([
    ['favicon-16x16.png', 'image/png'],
    ['favicon-32x32.png', 'image/png'],
    ['swagger-ui-bundle.js', 'application/javascript; charset=utf-8'],
    ['swagger-ui-bundle.js.map', 'application/json; charset=utf-8'],
    ['swagger-ui.css', 'text/css; charset=utf-8'],
    ['swagger-ui.css.map', 'application/json; charset=utf-8'],
  ])(`should serve ${prefix}%s using mime type %s`, async (p, mime) => {
    const app = new Koa();
    app.use(koas(document, [specHandler(), swaggerUI({ url })]));
    await setTestApp(app);
    const response = await request.get<Buffer>(`${prefix}${p}`, { responseType: 'arraybuffer' });
    expect(response.headers['content-type']).toBe(mime);
    expect(response.data.equals(assets[p])).toBe(true);
  });
});

describe('defaults', () => {
  it('should render index.html', async () => {
    const app = new Koa();
    app.use(koas(document, [specHandler(), swaggerUI()]));
    await setTestApp(app);
    const response = await request.get('/');
    expect(response.headers['content-type']).toBe('text/html; charset=utf-8');
    expect(response.data).toMatchSnapshot();
  });
});

describe('trailing slash', () => {
  it('should render index.html', async () => {
    const app = new Koa();
    app.use(koas(document, [specHandler(), swaggerUI({ url: '/prefix/' })]));
    await setTestApp(app);
    const response = await request.get('/prefix/');
    expect(response.headers['content-type']).toBe('text/html; charset=utf-8');
    expect(response.data).toMatchSnapshot();
  });
});

describe('no trailing slash', () => {
  it('should render index.html', async () => {
    const app = new Koa();
    app.use(koas(document, [specHandler(), swaggerUI({ url: '/prefix' })]));
    await setTestApp(app);
    const response = await request.get('/prefix');
    expect(response.headers['content-type']).toBe('text/html; charset=utf-8');
    expect(response.data).toMatchSnapshot();
  });
});
