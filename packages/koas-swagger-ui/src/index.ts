import { readFile } from 'node:fs/promises';
import { join, parse } from 'node:path';

import { type Plugin } from 'koas-core';
import 'koas-spec-handler';
import { absolutePath } from 'swagger-ui-dist';

import { SwaggerUIBundlePlugin } from './plugins.js';
import { SwaggerUIBundlePreset } from './presets.js';
import { relativeRequestPath } from './utils.js';

export const SwaggerUIBundle = {
  plugins: SwaggerUIBundlePlugin,
  presets: SwaggerUIBundlePreset,
};

const render = (
  title: string,
  url: string,
  presets: string[],
  plugins: string[],
): string => `<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <title>${title}</title>
    <link rel="stylesheet" type="text/css" href="./swagger-ui.css" />
    <link rel="icon" type="image/png" href="./favicon-32x32.png" sizes="32x32" />
    <link rel="icon" type="image/png" href="./favicon-16x16.png" sizes="16x16" />
    <style>
      html {
        overflow-y: scroll;
      }

      *,
      *:before,
      *:after {
        box-sizing: inherit;
      }

      body {
        margin: 0;
        background: #fafafa;
      }
    </style>
  </head>
  <body>
    <div id="swagger-ui"></div>
    <script src="./swagger-ui-bundle.js"></script>
    <script>
      SwaggerUIBundle({
        url: ${JSON.stringify(url)},
        dom_id: '#swagger-ui',
        deepLinking: true,
        displayOperationId: true,
        presets: [${presets.join(', ')}],
        plugins: [${plugins.join(', ')}],
        layout: 'BaseLayout',
      });
    </script>
  </body>
</html>`;

const paths = [
  'favicon-16x16.png',
  'favicon-32x32.png',
  'oauth2-redirect.html',
  'swagger-ui-bundle.js',
  'swagger-ui-bundle.js.map',
  'swagger-ui.css',
  'swagger-ui.css.map',
];

export interface SwaggerUIOptions {
  /**
   * The Swagger UI plugins to use.
   */
  plugins?: SwaggerUIBundlePlugin[];

  /**
   * The Swagger UI presets to use.
   */
  presets?: SwaggerUIBundlePreset[];

  /**
   * The URL from which to serve Swagger UI.
   */
  url?: string;
}

/**
 * Serve Swagger UI for the Koas API.
 *
 * @param options The options for Swagger UI.
 * @returns Koas middleware that servers Swagger UI
 */
export function swaggerUI({
  plugins = [SwaggerUIBundlePlugin.DownloadUrl],
  presets = [SwaggerUIBundlePreset.apis],
  url = '/',
}: SwaggerUIOptions = {}): Plugin {
  const pathMap = new Map(
    paths.map<[string, [string, string]]>((p) => [
      relativeRequestPath(url, p),
      [join(absolutePath(), p), parse(p).ext],
    ]),
  );

  return ({ document, runAlways }) =>
    runAlways(async (ctx, next) => {
      const { specURL } = ctx.openApi;
      if (ctx.path === url) {
        ctx.body = render(document.info?.title || 'SwaggerUI', specURL, presets, plugins);
        ctx.type = 'html';
        return;
      }
      if (pathMap.has(ctx.path)) {
        const [file, type] = pathMap.get(ctx.path);
        ctx.body = await readFile(file);
        ctx.type = type;
        return;
      }
      return next();
    });
}
