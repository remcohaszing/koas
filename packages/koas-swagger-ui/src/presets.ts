export enum SwaggerUIBundlePreset {
  apis = 'SwaggerUIBundle.presets.apis',
  base = 'SwaggerUIBundle.presets.base',
}
