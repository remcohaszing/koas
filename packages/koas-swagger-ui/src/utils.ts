/**
 * Resolve a file relative to a URL path root.
 *
 * @param root The URL root.
 * @param file The file to resolve
 * @returns The resolve URL path
 */
export function relativeRequestPath(root: string, file: string): string {
  if (root.endsWith('/')) {
    return `${root}${file}`;
  }
  return root.split('/').slice(0, -1).concat(file).join('/');
}
